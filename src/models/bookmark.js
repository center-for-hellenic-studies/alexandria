import mongoose from 'mongoose';
import shortid from 'shortid';

const BookmarkModel = mongoose.Schema({
	_id: {
		type: String,
		default: shortid.generate,
	},
	paragraphIndex: {
		type: Number,
		required: true,
	},
	userId: {
		type: String,
		ref: 'Users',
	},
	workId: {
		type: String,
		required: true,
	},
	workSource: {
		type: String,
		required: true,
	},
});

const Bookmark = mongoose.model('Bookmark', BookmarkModel);

export default Bookmark;
