import mongoose from 'mongoose';
import shortid from 'shortid';


const GroupModel = new mongoose.Schema({
	_id: {
		type: String,
		default: shortid.generate
	},

	name: {
		type: String,
		index: true,
	},

	desc: {
		type: String,
	},

	admins: {
		type: [{ type: String, ref: 'User' }],
	},

	members: {
		type: [{ type: String, ref: 'User' }],
	}
});


const Group = mongoose.model('Group', GroupModel);

export default Group;

