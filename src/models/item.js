import mongoose from 'mongoose';
import shortid from 'shortid';

// plug-ins
import timestamp from 'mongoose-timestamp';
import URLSlugs from 'mongoose-url-slugs';
import mongoosePaginate from 'mongoose-paginate';

// models
import Collection from './collection';
import File from './file';


const {Schema} = mongoose;


export const metadataTypes = ['text', 'number', 'date', 'place', 'media', 'item', 'color'];


export const MetadataSchema = new Schema({
	_id: {
		type: String,
		default: shortid.generate
	},
	type: {
		type: String,
		enum: metadataTypes,
		required: true,
		default: 'text',
	},
	value: {
		type: String,
		required: true,
	},
	valueJSON: {
		type: Schema.Types.Mixed,
		required: false,
	},
	label: {
		type: String,
		required: true,
	}
});

/**
 * Item base schema
 * @type {Schema}
 */
const ItemSchema = new Schema({
	_id: {
		type: String,
		default: shortid.generate
	},
	title: {
		type: String,
		required: true,
		trim: true,
		index: true,
		text: true,
	},
	projectId: {
		type: String,
		ref: 'Project',
		index: true
	},
	collectionId: {
		type: [String],
		ref: 'Collection',
		index: true
	},
	description: {
		type: String,
		// index: true,
		// text: true,
	}, // mongo only allows one text index per collection, currently using title for the index
	tags: {
		type: [String],
		// text: true,
	},
	metadata: [MetadataSchema],
	private: {
		type: Boolean,
		default: false,
	},
	generatedBy: {
		type: String,
	},
	shouldUseMachineLearning: { // should this item be processed by machine learning tools
		type: Boolean,
		default: false,
	}
}, {
	timestamps: true,
	versionKey: process.env.DEFAULT_VERSIONKEY || '_vk',
});

// add slug (slug)
ItemSchema.plugin(URLSlugs('title', {
	indexUnique: false,
}));

// item indexing for full text search
// TODO: not sure yet if this is currently implemented in a performant way
// ItemSchema.index({'$**': 'text'});

/**
 * Statics
 */

/**
 * Find all items belonging to a collection
 * @param  {String} collectionId 	Collection id
 * @return {Promise}              	(Promise) Array of found items
 */

ItemSchema.statics.findByCollectionId = function findByCollectionId(collectionId) {
	return this.find({ collectionId }).select({ _id: 1 });
};


/**
 * Methods
 */

ItemSchema.methods.validateUser = function validateUser(userId) {
	return Collection.isUserAdmin(this.collectionId, userId);
};

/**
 * Middleware
 */

ItemSchema.pre('remove', function(next) {
	return File.deleteMany({ itemId: this._id }).then(res => next(res));
});

/**
 * Item mongoose model
 * @type {Object}
 */
const Item = mongoose.model('Item', ItemSchema);

export default Item;
export { ItemSchema };
