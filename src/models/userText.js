import mongoose from 'mongoose';
import timestamp from 'mongoose-timestamp';
import shortid from 'shortid';
import { RevisionModel } from './revision';

const UserTextModel = new mongoose.Schema({
	_id: {
		type: String,
		default: shortid.generate,
	},

	urn: {
		type: String,
		index: true,
		required: true,
		description:
			'can be CTS URN (eg. urn:cts:greekLit:tlg0012.tlg001:2.1) or full URN with edition info (eg.urn:cts:greekLit:tlg0012.tlg001.perseus-grc2)',
	},

	revisions: {
		required: true,
		type: [RevisionModel],
		description:
			'every time this piece of text gets updated with a new version of text, push to this array',
	},

	contentType: {
		type: String,
		required: true,
		enum: ['translation', 'edition'],
	},

	status: {
		type: String,
		optional: true,
		default: 'private',
		enum: ['public', 'private'],
		description:
			'private (default): only seen by the creator; or public: seen by everyone',
	},

	userID: {
		type: String,
		ref: 'Users',
		description: 'the creator of this piece of text',
	},

	commentaryID: {
		type: String,
		ref: 'Projects',
		description:
			'the ID of the Commentary(project/tenant) this piece of text belongs to',
	},
});

UserTextModel.plugin(timestamp);

const UserText = mongoose.model('UserText', UserTextModel);

export default UserText;
