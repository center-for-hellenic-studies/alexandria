import cors from 'cors';


export default function corsSetup(app) {

	const whitelist = ['http://alexandria.archimedes.digital', 'http://localhost:3001'];

	if (process.env.NODE_ENV === 'development') {
		whitelist.push(process.env.CLIENT_SERVER);
	}

	const corsOptions = {
		origin: (origin, callback) => {
			// TODO: examine why sometimes origin isn't defined

			if (
				origin
				&& (
					origin.endsWith('archimedes.digital')
					|| origin.endsWith('localhost:3000')
					|| origin.endsWith('localhost:3001')
					|| ~whitelist.indexOf(origin)
				)
			) {
				// callback(null, true);
			} else {
				// callback(new Error('Not allowed by CORS'));
			}

			callback(null, true);
		},
		credentials: true,
	};

	// set cors
	app.use(cors(corsOptions));

	// Enable preflight
	app.options('*', cors());
}
