// main
import express from 'express';
import path from 'path';
import webpack from 'webpack';

// middleware
import bodyParser from 'body-parser';
import session from 'express-session';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import webpackDevMiddleware from 'webpack-dev-middleware';
import httpContext from 'express-http-context';

// dotenv
import dotenvSetup from './dotenv';

// mongoDB
import dbSetup, { storeSetup } from './mongoose';

// authentication
import authSetup from './authentication';

// cors
import corsSetup from './cors';

// graphQL
import setupGraphql from './graphql';

// S3
import s3Setup from './s3';

// Redis
import redisSetup from './redis';

// OAuth setup
import oauthSetup from './oauth';

// email
// import EmailManager from './email';

// Routes
import authenticationRouter from './authentication/routes';

import ReadingEnvironment from './ReadingEnvironment';

// SSR routes
import texts from './routes/texts';
import commentaries from './routes/commentaries';
import pages from './routes/pages';
import ssr from './routes/ssr';

// environment variables setup
dotenvSetup();

const app = express();

const db = dbSetup();

const redisClient = redisSetup();

app.set('port', process.env.PORT || 3001);

if (process.env.NODE_ENV === 'production') {
	app.use(express.static('client/build'));
}

app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(
	bodyParser.urlencoded({
		extended: false,
		limit: '50mb',
	})
);

if (process.env.NODE_ENV === 'development') {
	/* eslint-disable */
	const config = require(path.resolve(__dirname, '..', 'webpack.config.js'));
	const compiler = webpack(config);
	app.use(
		webpackDevMiddleware(compiler, {
			hot: true,
			publicPath: config.output.publicPath,
		})
	);
	app.use(require('webpack-hot-middleware')(compiler));
	/* eslint-enable */
}

// session:
app.use(
	session({
		secret: process.env.SESSION_SECRET || 'secret',
		resave: false,
		saveUninitialized: false,
		store: storeSetup(session),
	})
);

// http context
app.use(httpContext.middleware);

// CORS setup
corsSetup(app);

// Authentication setup
authSetup(app, redisClient);

// GraphQl setup
setupGraphql(app);

// S3 setup
s3Setup(app);

// OAuth setup
oauthSetup(app);

app.use(express.static(path.resolve(__dirname, '..', 'public')));
app.use(express.static(path.resolve(__dirname, '..', 'vendor')));
app.use(express.static(path.resolve(__dirname, 'public')));

const HEADERS_ORIGIN_REGEX = /https?:\/\//;

// http context on routes
app.use((req, _res, next) => {
	if ('authorization' in req.headers) {
		const token = req.headers.authorization.replace('JWT ', '');
		httpContext.set('token', token);
		httpContext.set('gqlauth', req.headers.authorization);
		httpContext.set(
			'reqHost',
			req.headers.origin.replace(HEADERS_ORIGIN_REGEX, '')
		);
	}
	next();
});

// Routes
app.use('/auth', authenticationRouter);

app.use('/read', ReadingEnvironment);

// SSR Routes
app.use('/texts', texts);
app.use('/commentaries', commentaries);
app.use('/ssr', ssr);

app.use('/', pages);

function listen() {
	const server = app.listen(app.get('port'), () => {
		console.info(`Application listening on port ${app.get('port')}`);
		server.keepAliveTimeout = 0;
	});
}

export { app, db, listen };
