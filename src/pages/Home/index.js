// Dev notes: This is where some SSR magic happens.  
// By including the Home component, or any other page-level container, via an index.js like this,
// the application can provide a live version of the SSR'd component.

// We're currently using two different components for the ssr and the csr, 
// but once build process for ssr is figured out, we could use the same component.

// This works by initiating this component on the same DOM element as the loader rendered via the express route.

// These files are bundled using webpack and new page-level components need to be defined as entry points in webpack.config.js
// They also need to be linked in the html response of the route by defining the jsFileName variable in the html handlebars template.


import React from 'react';
import { hydrate } from 'react-dom';
import Home from './home';
import { ApolloProvider } from 'react-apollo';
import client from '../../client';


hydrate(
	<ApolloProvider client={client}>
		<Home /></ApolloProvider>, document.getElementById('HomeEle'));

