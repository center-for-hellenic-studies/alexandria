// Dev notes: This component acts as a page-level container.  
// It is built using webpack, defined in webpack.config.js as an entry point, and then linked into the server-side rendered html response defined in /src/routes/pages/js
// This is an example of a layout.  It's title and meta tags are set in the route's html response and the page-specific bundle is linked via a Handlebar variable called jsFileName.

import React from 'react';
import Cover from '@bit/archimedes_digital.orpheus.cover';
import ArchiveCoverContainer from '@bit/archimedes_digital.orpheus.archive.archive-cover-container';

class Home extends React.Component {
	constructor() {
		super();
		
	}

	componentDidMount() {
		console.log('\n\n\n\n ========= componentDidMount home');
	}

	componentDidUpdate() {
		console.log('\n\n\n\n ========= componentDidUpdate home');
	}

	componentWillReceiveProps() {
		console.log('\n\n\n\n ========= componentWillReceiveProps home');
	}


	render() {
		console.log('\n\n\n\n ========= rendering home');
		return (
			<div>
				<p><em>This is the Cover component. It is a good example of how to use a Component from Orpheus.</em></p>
				<Cover title={'Test Title for Cover Element'} />
				<hr />
				<p><em>This is the Archive Cover component.  It is a good exaple of how to use a Container from Orpheus.  You'll likely notice console errors because the graphql query in the Orpheus container doesn't exactly match this API's yet.</em></p>
				<ArchiveCoverContainer />
				<hr />
			</div>
		);
	}
}
export default Home;
