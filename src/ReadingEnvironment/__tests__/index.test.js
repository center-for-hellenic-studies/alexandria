import bodyParser from 'body-parser';
import express from 'express';
import nock from 'nock';
import path from 'path';
import supertest from 'supertest';

import ReadingEnvironment from '../index';

const app = express();

app.use(bodyParser.json({ limit: '50mb' }));
app.use(express.static(path.resolve(__dirname, '../../..', 'public')));

app.use('/', ReadingEnvironment);

const response = {
	data: {
		CHS_article: {
			articleContent: '<p>article</p>',
			title: 'Test Article',
		},
	},
};

describe('GET /:source/:id', () => {
	beforeEach(() => {
		nock('https://api.chs.harvard.edu')
			.post('/graphql')
			.reply(200, response);
	});

	describe('Accepts: application/json', () => {
		it('responds with the correct JSON', () =>
			supertest(app)
				.get('/ahcip/6199')
				.set('Accept', 'application/json')
				.expect('Content-Type', /json/)
				.expect(200)
				.then(response => {
					const { data } = response.body;

					expect(data.text.content).toEqual('<p>article</p>');
					expect(data.text.title).toEqual('Test Article');
				}));
	});

	describe('Accepts: text/html', () => {
		it('responds with the html view', () =>
			supertest(app)
				.get('/ahcip/6199')
				.set('Accept', 'text/html')
				.expect('Content-Type', /html/)
				.expect(200)
				.then(response => {
					expect(response.text.includes('<p>article</p>')).toBeTruthy();
					expect(response.text.includes('Test Article')).toBeTruthy();
				}));
	});
});
