/**
 * @prettier
 */

import { Router } from 'express';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import fs from 'fs';
import handlebars from 'handlebars';
import path from 'path';
import request from 'request-promise';

import App from '../client/App';
import Bookmark from '../models/bookmark';

const router = Router();
const htmlTemplate = handlebars.compile(
	fs.readFileSync(path.resolve(__dirname, 'views/layout.hbs'), 'utf-8')
);

const textSources = {
	ahcip: async articleId => {
		const opts = {
			uri: process.env.AHCIP_URL || 'https://api.chs.harvard.edu/graphql',
			method: 'POST',
			json: true,
			body: {
				query: `{
					CHS_article(id: "${articleId}") {
						id
						title
						articleContent
					}
				}`,
			},
		};

		const body = await request(opts);
		const {
			data: {
				CHS_article: { articleContent, title },
			},
		} = body;

		return { content: articleContent, id: articleId, source: 'ahcip', title };
	},
	chs: async urn => {
		const opts = {
			uri:
				process.env.CHS_TEXTSERVER_URL || 'http://text.chs.harvard.edu/graphql',
			method: 'POST',
			json: true,
			body: {
				query: `{
					works(urn:"${urn}") {
						id
						english_title
						original_title
						version {
							id
							title
							urn
						}
						language {
							id
							title
						}
						textNodes {
							id
							location
							text
						}
					}
				}`,
			},
		};

		const body = await request(opts);
		const {
			data: { works },
		} = body;
		const work = works[0];

		// template text node strings
		let content = work.textNodes
			.map(
				tn => `<span class="location">${tn.location.join('.')}</span>${tn.text}`
			)
			.join('</span><span class="textnode">');

		// add beginning and ending paragraphs
		content = `<span class="textnode">${content}</span>`;

		return {
			content,
			id: urn,
			source: 'chs',
			title: work.original_title,
		};
	},
};

router.post('/:source/:id/bookmarks', async (req, res) => {
	const { id, source } = req.params;
	const reqBody = req.body;
	let bookmark = new Bookmark({
		...reqBody.bookmark,
		workId: id,
		workSource: source,
	});

	bookmark.save();

	res.json({ data: { bookmark } });
});

router.get('/:source/:id', async (req, res) => {
	const text = await textSources[req.params.source](req.params.id);
	const acceptsJson = req.get('accept').includes('application/json');

	if (acceptsJson) {
		res.json({ data: { text } });
	} else {
		const body = ReactDOMServer.renderToString(<App text={text} />);
		res.send(htmlTemplate({ body, props: JSON.stringify({ text }) }));
	}
});

router.get('/', (_req, res) => {
	const body = ReactDOMServer.renderToString(<App />);
	res.send(htmlTemplate({ body, props: JSON.stringify({}) }));
});

export default router;
