import { Router } from 'express';
import request from 'request-promise';

const router = Router();

const getTextsAll = async (options) => {
	const q = {
		uri: 'http://localhost:8080/graphql',
		method: 'POST',
		json: true,
		body: {
			query: `{
				TEXT_works(
					limit:${options.limit}
					offset:${options.offset}
				){
					id
					english_title
					original_title
					version {
						id
						title
						urn
					}
					language {
						id
						title
					}
					textNodes {
						id
						text
					}
				}
			}`,
		},
	};

	const body = await request(q);
	const {
		data: { TEXT_works },
	} = body;

	return TEXT_works; // eslint-disable-line
};

router.get('/all', async (req, res) => {

	const options = {
		limit: req.query.limit || 10,
		offset: req.query.offset || 0,
	};

	const textsAll = await getTextsAll(options);

	res.send(textsAll);
});

export default router;
