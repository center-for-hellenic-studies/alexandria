import express from 'express';
import React from 'react';
import hbs from 'handlebars';
import { renderToString } from 'react-dom/server';

// import the model to get basic project data in order to render skeleton page with social and seo meta tags
import Project from '../models/project';

// import a loading element that will share the DOM id with the page-level container
import HomeLoader from '../pages/HomeLoader';

const router = express.Router();
const htmlTemplate = `
<html>
<head><title>{{title}}</title></head>
<body>
<div id="{{{reactElementId}}}">{{{reactEle}}}</div>
<script src="/js/{{{jsFileName}}}.js" charset="utf-8"></script>
<script src="/js/vendor.js" charset="utf-8"></script>
</body>
</html>
`;

router.get('/', async (req, res) => {
	const hostname = req.subdomains[0];

	Project.findOne({hostname: hostname}, function(err, result) {
		const hbsTemplate = hbs.compile(htmlTemplate);
		const reactComp = renderToString(<HomeLoader />);
		const htmlToSend = hbsTemplate({ 
			reactEle: reactComp, 
			reactElementId: 'HomeEle', // needs to be same as in /src/pages/Home/index.js
			jsFileName: 'home', // defined as entry in webpack.config.js
			title: 'Alexandria (todo update title and metatags here)' 
		});
		res.send(htmlToSend);
	});
}); 

export default router;
