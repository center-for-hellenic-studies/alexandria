import { Router } from 'express';
import { GraphQLClient } from 'graphql-request';
import httpContext from 'express-http-context'; // for accessing jwt token

const router = Router();

const client = new GraphQLClient('http://localhost:8080/graphql', {
	headers: {
	},
});

const getCommentariesAll = async (options) => {
	const query = `{
		projectMany(
			limit:${options.limit}
			skip:${options.skip}
		){
			title
			subtitle
			status
			description
			hostname
			email
			url
			address
			phone
			slug
			users {
				userId
				role
				status
			}
		}
	}`;
	return client.request(query);
};


const createCommentary = async (project) => {
	const clientTmp = new GraphQLClient('http://localhost:8080/graphql', {
		headers: {
			authorization: httpContext.get('gqlauth'),
		},
	});
	const query = `mutation projectCreate($project: ProjectInput!){
		projectCreate(
			project: $project
		){
			title
			subtitle
			status
			description
			hostname
			email
			url
			address
			phone
			slug
			users {
				userId
				role
				status
			}
		}
	}`;
	const variables = {
		project: project,
	};
	return clientTmp.request(query, variables);
};

router.get('/all', async (req, res) => {
	const commentariesAll = await getCommentariesAll({
		limit: req.query.limit || 10,
		skip: req.query.skip || 0,
	});
	res.send(commentariesAll);
});

router.post('/create', async (req, res) => {
	const commentaryCreated = await createCommentary(req.body.commentary);
	res.send(commentaryCreated);
});

export default router;
