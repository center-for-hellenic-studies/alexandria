import express from 'express';
import React from 'react';
import hbs from 'handlebars';
import { renderToString } from 'react-dom/server';

// react components
import App from '../components/app';

// a router with react routes
const router = express.Router();

// root route of /ssr
router.get('/', async (req, res) => {

	// template with handlebars
	const htmlTemplate = `
	<html>
	<head><title>SSR</title></head>
	<body>
	<h1>Server Side Render</h1>
	<div id="reactEle">{{{reactEle}}}</div>
	<script src="/app.js" charset="utf-8"></script>
	<script src="/vendor.js" charset="utf-8"></script>
	</body>
	</html>
	`;
	const hbsTemplate = hbs.compile(htmlTemplate);

	// render react component App to string
	const reactComp = renderToString(<App />);

	// render html to be sent
	const htmlToSend = hbsTemplate({ reactEle: reactComp });

	// send
	res.send(htmlToSend);
});

export default router;
