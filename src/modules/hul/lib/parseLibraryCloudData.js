import { humanize, trim } from 'underscore.string';
import shortid from 'shortid';

const getLibraryCloudTitle = (item) => {
	let title = '';
	if (item.titleInfo && item.titleInfo.title) {
		title = item.titleInfo.title;
	} else {
		title = item.titleInfo;
	}

	title = title.replace('Charlie Archive at the Harvard Library.', '');
	title = title.replace('Charlie Archive Collection at the Harvard Library.', '');
	title = title.replace('[Charlie Archive Collection at the Havard Library]', '');
	title = title.replace('[Charlie Archive Collection at the Harvard Library]', '');
	title = title.replace('Digital images.', '');
	title = trim(title);
	title = trim(title, '.');

	return title;
};

const getLibraryCloudName = (item) => {
	let name = '';

	if (Array.isArray(item.name)) {
		item.name.forEach((_name) => {
			if (Array.isArray(_name.namePart)) {
				_name.namePart.forEach((_n) => {
					if (typeof _n === 'string') {
						name = `${name} ${_n}`;
					}
				});
			} else {
				name = `${name} ${_name.namePart}`;
			}
		});
	} else if (item.name) {
		if (Array.isArray(item.name.namePart)) {
			item.name.namePart.forEach((_n) => {
				if (typeof _n === 'string') {
					name = `${name} ${_n}`;
				}
			});
		} else {
			name = `${name}${item.name.namePart}`;
		}
	}

	return trim(name);
};

const getLibraryCloudDescription = (item) => {
	const description = '';
	return description;
};

const getLibraryCloudRecordIdentifier = (item) => {
	let recordIdentifier = '';

	recordIdentifier = item.recordInfo.recordIdentifier['#text'];

	return recordIdentifier;
};


const getLibraryCloudImgSrc = (item) => {
	let imgSrc = false;

	// TODO: adapt with DRS extension
	// file delivery url
	// same as Full image
	// parameters to get the thumbnail

	if (item.relatedItem
		&& item.relatedItem.length
		&& item.relatedItem[0].location
		&& item.relatedItem[0].location.length > 1
		&& item.relatedItem[0].location[0].url
		&& item.relatedItem[0].location[0].url.length > 1
	) {
		imgSrc = item.relatedItem[0].location[0].url[0]['#text'];
	}

	return imgSrc;
};

const getLibraryCloudImgThumbnailSrc = (item) => {
	let imgSrc = false;

	if (item.relatedItem
		&& item.relatedItem.length
		&& item.relatedItem[0].location
		&& item.relatedItem[0].location.length > 1
		&& item.relatedItem[0].location[0].url
		&& item.relatedItem[0].location[0].url.length > 1
	) {
		imgSrc = item.relatedItem[0].location[0].url[1]['#text'];
	}

	return imgSrc;
};

const getLibraryCloudMetadata = (item) => {
	const metadata = [];

	const name = getLibraryCloudName(item);
	if (name) {
		metadata.push({
			_id: shortid.generate(),
			label: 'Creator',
			type: 'text',
			value: name,
		});
	}

	if (item.typeOfResource) {
		metadata.push({
			_id: shortid.generate(),
			label: 'Type of Resource',
			type: 'text',
			value: humanize(item.typeOfResource),
		});
	}

	if (item.genre) {
		metadata.push({
			_id: shortid.generate(),
			label: 'Genre',
			type: 'text',
			value: humanize(item.genre),
		});
	}

	if (item.location) {
		if (item.location.physicalLocation) {
			if (item.location.physicalLocation['#text']) {
				metadata.push({
					_id: shortid.generate(),
					label: 'Physical Location',
					type: 'text',
					value: item.location.physicalLocation['#text'],
				});
			}
		}

		// More location information?
	}

	if (item.originInfo && item.originInfo.dateCreated) {
		const dateAdded = false;
		if (Array.isArray(item.originInfo.dateCreated)) {
			item.originInfo.dateCreated.forEach((dateVal) => {
				if (dateVal === 'string' && !dateAdded) {
					metadata.push({
						_id: shortid.generate(),
						label: 'Date Created',
						type: 'text',
						value: dateVal,
					});
				}
			});
		}

	}

	const recordIdentifier = getLibraryCloudRecordIdentifier(item);
	if (recordIdentifier) {
		metadata.push({
			_id: shortid.generate(),
			label: 'Record Identifier (HOLLIS+)',
			type: 'text',
			value: recordIdentifier,
		});
	}

	return metadata;
};

const getLibraryCloudTags = (item) => {
	const tags = [];

	if (item.subject && item.subject.topic) {
		if (item.subject.topic === 'string') {
			tags.push(item.subject.topic.toLowerCase());
		} else if (item.subject.topic['#text']) {
			tags.push(item.subject.topic['#text'].toLowerCase());
		}
	}

	return tags;
};


export {
	getLibraryCloudTitle, getLibraryCloudName, getLibraryCloudRecordIdentifier, getLibraryCloudImgSrc,
	getLibraryCloudImgThumbnailSrc, getLibraryCloudMetadata, getLibraryCloudDescription, getLibraryCloudTags,
};

export default getLibraryCloudName;
