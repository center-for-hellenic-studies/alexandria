import winston from 'winston';

// models
import Item from '../../../models/item';
import Collection from '../../../models/collection';

// lib
import saveLibraryCloudFiles from './saveLibraryCloudFiles';
import saveManifest from './saveManifest';
import makeItemTitleFromCollection from '../lib/makeItemTitleFromCollection';
import cloudVisionDataToTags from '../lib/cloudVisionDataToTags';
import {
	getLibraryCloudTitle,
	getLibraryCloudDescription,
	getLibraryCloudMetadata,
	getLibraryCloudTags,
} from '../lib/parseLibraryCloudData';



const saveLibraryCloudItem = async (record) => {

	// console.log('...record', record);

	let collection;
	let shouldSetCollectionCoverImage = false;
	const projectId = process.env.DB_INGEST_CAHL_PROJECT_ID || 'Sk0_W3GYm';

	const collectionTitle = getLibraryCloudTitle(record);
	collection = await Collection.findOne({
		title: collectionTitle,
		projectId,
	});

	if (!collection) {
		collection = new Collection({
			title: collectionTitle,
			projectId,
			description: `Items submitted to the Charlie Archive at the Harvard Libraries by ${collectionTitle}`,
		});
		await collection.save();
		shouldSetCollectionCoverImage = true;
	}

	const item = new Item({
		title: await makeItemTitleFromCollection(collection),
		description: getLibraryCloudDescription(record),
		metadata: getLibraryCloudMetadata(record),
		tags: await getLibraryCloudTags(record),
		private: false,
		collectionId: [collection._id],
		projectId,
	});

	await item.save();

	winston.info(`Created item ${item._id}`);
	let files;
	try {
		winston.info(`Saving files for item ${item._id}`);
		files = await saveLibraryCloudFiles(item, record);
	} catch (err) {
		winston.error(err);
	}

	try {
		if (files) {
			winston.info(`Saving manifest for item ${item._id}`);
			const manifest = await saveManifest(item, files);
		}
	} catch (err) {
		// winston.error(err);
	}

	// save tags
	winston.info(`Updating tags for item ${item._id}`);
	const tags = cloudVisionDataToTags(files);
	const _itemTags = tags.concat(item.tags);
	await Item.update({ _id: item._id, }, {
		$set: {
			tags: _itemTags,
		},
	});

	// save collection cover
	if (shouldSetCollectionCoverImage && files && files[0] && files[0].name) {
		winston.info(`Updating cover for collection ${collection._id}`);
		await Collection.update({ _id: collection._id }, {
			$set: {
				coverImage: files[0].name,
			},
		});
	}
};

export default saveLibraryCloudItem;
