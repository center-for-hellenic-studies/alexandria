import winston from 'winston';
import axios from 'axios';
import _ from 'underscore';

// models
import Item from '../../../models/item';
import File from '../../../models/file';
import Manifest from '../../../models/manifest';
import ItemRaw from '../../../models/itemraw';
import Collection from '../../../models/collection';

// lib
import saveLibraryCloudItem from './saveLibraryCloudItem';


// get a single page of library items
const getLibraryItems = async (limit, start) => {
	let url = 'https://api.lib.harvard.edu/v2/items.json?';
	const urlParams = [];

	// Query for the CAHL Collection subject
	urlParams.push('subject_exact=Charlie%20Hebdo%20Attack,%20Paris,%20France,%202015');

	// Pass search query to title to prevent conflict with subject_exact query to collection
	urlParams.push(`start=${start}`);
	urlParams.push(`limit=${limit}`);
	urlParams.push('inDRS=true');
	urlParams.push('accessFlag=P');
	url += urlParams.join('&');

	// Get items from api
	const response = await axios.get(url);
	const items = response.data.items.mods;

	// Save items
	for (let i = 0; i < items.length; i += 1) {
	// for (let i = 0; i < 2; i += 1) {
		winston.info(`Ingesting ${start + i}...`);
		await saveLibraryCloudItem(items[i]); // eslint-disable-line
	}

	return true;
};

// Crawl Library Cloud API and save all items locally in Mongo
const libraryCloudIngest = async () => {
	// remove all existing for project
	const projectId = process.env.DB_INGEST_CAHL_PROJECT_ID || 'Sk0_W3GYm';
	await Item.remove({ projectId, });
	await File.remove({ projectId, });
	await Collection.remove({ projectId, });
	await Manifest.remove({ title: /.*Charlie.*/ });

	// crawl api
	const limit = 50;
	let total = 0;
	let url = 'https://api.lib.harvard.edu/v2/items.json?';
	const urlParams = [];

	// Query for the CAHL Collection subject
	urlParams.push('subject_exact=Charlie%20Hebdo%20Attack,%20Paris,%20France,%202015');
	urlParams.push('inDRS=true');
	urlParams.push('accessFlag=P');
	url += urlParams.join('&');

	// Get pagination information:
	const res = await axios.get(url);
	total = res.data.pagination.numFound;
	console.log(`Retrieving results for ${total} objects:`);

	// Get paginated data
	for (let i = 0; i < (total / limit); i += 1) {
	// for (let i = 0; i < 1; i += 1) {

		// preview mode of db:ingest
		if (process.env.DB_INGEST_PREVIEW && i > 0) {
			winston.info('Preview Mode enabled, finishing after the first page.');
			break;
		}

		winston.info(`Page :${i}`);
		try {
			await getLibraryItems(limit, i * limit); // eslint-disable-line
		} catch (err) {
			winston.error(err);
		}
	}

	// Return success message
	return 'Ingest successful';
};


//
// retrieve raw items from LibCloud
//

const composeGetURL = () => {
	// crawl api
	let url = 'https://api.lib.harvard.edu/v2/items.json?';
	const urlParams = [];

	// Query for the CAHL Collection subject
	urlParams.push('subject_exact=Charlie%20Hebdo%20Attack,%20Paris,%20France,%202015');
	urlParams.push('inDRS=true');
	urlParams.push('accessFlag=P');
	url += urlParams.join('&');
	return url;
};

const getTotalItemRawCount = async (url) => {
	const res = await axios.get(url);
	return res.data.pagination.numFound;
};

const getItemRawByPage = async (start, limit) => {
	// compose get by page URL
	let url = composeGetURL();
	const urlParams = [];
	urlParams.push(`start=${start}`);
	urlParams.push(`limit=${limit}`);
	url += `&${urlParams.join('&')}`;

	// send GET
	const response = await axios.get(url);
	let items = response.data.items.mods;
	if (!Array.isArray(items)) {
		items = [items];
	}

	// save to DB
	const saveItemsRaw = items.map(async(item) => {
		await saveLibraryItemRaw(item, process.env.DB_INGEST_CAHL_PROJECT_ID || 'Sk0_W3GYm'); // eslint-disable-line
	});
	await Promise.all(saveItemsRaw);
	winston.info(`Page of ItemsRaw retrieved:${start}-${start + limit}`);
};

const saveLibraryItemRaw = async (item, sourceId) => {
	// save raw item
	const itemRaw = new ItemRaw({
		sourceId: sourceId,
		rawData: item,
	});
	await itemRaw.save();
};

// Crawl Library Cloud API and save all raw items locally in Mongo
const libraryCloudIngestRaw = async () => {
	// remove all existing for project
	await ItemRaw.remove({sourceId: process.env.DB_INGEST_CAHL_PROJECT_ID || 'Sk0_W3GYm'});

	// get total count
	const total = await getTotalItemRawCount(composeGetURL());
	winston.info(`Retrieving results for ${total} objects:`);

	// get by page
	const pageSize = 50;
	for (let i = 0; i < (total / pageSize); i += 1) {
		try {
			await getItemRawByPage(i * pageSize, pageSize); // eslint-disable-line
		} catch (error) {
			winston.info('GET failed, retry...');
			await getItemRawByPage(i * pageSize, pageSize); // eslint-disable-line
		}
		
	}

	// Return success message
	return 'Ingest ItemsRaw successful';
};


export default libraryCloudIngest;
export { libraryCloudIngestRaw };
