import fs from 'fs';
import AWS from 'aws-sdk';
import path from 'path';
import download from 'image-downloader';
import { slugify } from 'underscore.string';
import shortid from 'shortid';
import winston from 'winston';
import getExif from 'exif-async';

// lib
import {
	getLibraryCloudImgSrc,
} from '../lib/parseLibraryCloudData';
import saveManifest from './saveManifest';
import getImageGoogleCloudVisionData from '../lib/getImageGoogleCloudVisionData';

// models
import File from '../../../models/file';



const saveLibraryCloudFiles = async (item, record) => {
	// config AWS
	AWS.config.update({
		accessKeyId: process.env.AWS_ACCESS_KEY_ID,
		secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
	});

	// setup tmp dir
	const dir = './tmp';
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}

	// gather metadata for files to download
	const files = [];

	// for each related item in record
	for (let i = 0; i < record.relatedItem.length; i += 1) {
		const relatedItem = record.relatedItem[i];
		if (
			relatedItem.typeOfResource === 'still image'
      && relatedItem.location
      && Array.isArray(relatedItem.location)
      && relatedItem.location.length
      && relatedItem.location[0].url
		) {

			const file = {};

			// project
			file._id = shortid.generate();
			file.projectId = process.env.DB_INGEST_CAHL_PROJECT_ID || 'Sk0_W3GYm';
			file.itemId = item._id;
			file.type = 'image/jpeg';
			file.title = `${item.title} ${i}`;
			file.name = `${file._id}-${slugify(item.title)}_${i}.jpg`;

			// download image to tmp directory and upload to iiif image server
			let downloadUrl = relatedItem.location[0].url;
			
			if (Array.isArray(downloadUrl)) {
				downloadUrl = downloadUrl[0]['#text'];
			}
			const localPath = `./tmp/${file.name}`;

			


			winston.info('Downloading image to local');
			const dl = await download.image({ // eslint-disable-line
				url: downloadUrl,
				dest: localPath,
				timeout: 10 * 1000,
			});

			// exif
			console.log('...url', downloadUrl, localPath);
			try {
				const r = await getExif(localPath); // eslint-disable-line
				console.log('...r', r.exif, r.exif.MakerNote.toString('utf8'), r.exif.UserComment.toString('utf8'));
			} catch (err) {
				// console.log(err);
			}
			

			

			// Read in the file, convert it to base64, store to S3
			const key = localPath.replace('./tmp/', '');
			const fileData = fs.readFileSync(localPath);
			const base64data = new Buffer(fileData, 'binary');
			const s3 = new AWS.S3();
			/*
			await s3.upload({ // eslint-disable-line
				Bucket: process.env.AWS_BUCKET,
				Key: key,
				Body: base64data,
				ACL: 'public-read'
			}).promise();
			*/

			file.path = `https://iiif.orphe.us/${key}`;
			file.thumbPath = `https://iiif.orphe.us/${key}/full/90,/0/default.jpg`;

			/*
			try {
				winston.info(`Fetching tags for file ${key}`);
				file.cloudVisionDataRaw = await getImageGoogleCloudVisionData(`http://iiif.orphe.us/${key}/full/full/0/default.jpg`); // eslint-disable-line
			} catch (err) {
				winston.error(err);
			}
			*/

			const newFile = new File(file);
			await newFile.save(); // eslint-disable-line
			files.push(newFile);
		}
	}

	winston.info(`Saved files for item ${item._id}`);

	return files;
};


export default saveLibraryCloudFiles;
