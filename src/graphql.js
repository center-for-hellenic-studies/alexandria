import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import { formatError } from 'apollo-errors';
import { GraphQLSchema, execute, subscribe } from 'graphql';
import {
	makeRemoteExecutableSchema,
	mergeSchemas,
	introspectSchema,
	transformSchema,
	RenameTypes,
	RenameRootFields,
} from 'graphql-tools';
import { createApolloFetch } from 'apollo-fetch';
import { maskErrors } from 'graphql-errors';
import { createServer } from 'http';
// import { PubSub } from 'graphql-subscriptions';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import jwt from 'jsonwebtoken';

// authentication
import { jwtAuthenticate } from './authentication';

// graphql
import RootQuery from './graphql/queries/rootQuery';
import RootMutation from './graphql/mutations/rootMutation';
import RootSubscription from './graphql/subscriptions/rootSubscription';

// models
import User from './models/user';

// schema
import * as schemaBuilder from './graphql/schema';

/**
 * Create a remote schema for merging with local schema definition
 * via example Schema stitching from the repo mentioned here:
 * https://dev-blog.apollodata.com/graphql-schema-stitching-8af23354ac37
 */
const createRemoteSchema = async uri => {
	const fetcher = async ({ query, variables, operationName, context }) => {
		const fetchResult = await fetch(uri, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ query, variables, operationName }),
		});
		return fetchResult.json();
	};

	return makeRemoteExecutableSchema({
		schema: await introspectSchema(fetcher),
		fetcher,
	});
};

const validateRemoteSchemas = async remoteSchemaList => {
	const getValidRemoteSchemaList = remoteSchemaList.map(async remoteSchema => {
		// remote schema validation
		const schema = await createRemoteSchema(remoteSchema.url).catch(e => {
			console.error(`Could not link remote schema ${remoteSchema}`);
			console.error(e);
		});
		// namespace transform
		const transformedSchema = transformSchema(schema, [
			new RenameTypes(typeName => `${remoteSchema.namespace}_${typeName}`),
			new RenameRootFields(
				(op, fieldName) => `${remoteSchema.namespace}_${fieldName}`
			),
		]);
		return transformedSchema;
	});
	const validRemoteSchemaList = await Promise.all(getValidRemoteSchemaList);
	return validRemoteSchemaList.filter(remoteSchema => remoteSchema);
};

/**
 * Root schema
 * @type {GraphQLSchema}
 */
const RootSchema = new GraphQLSchema({
	query: RootQuery,
	mutation: RootMutation,
	// subscription: RootSubscription,
});

// mask error messages
// maskErrors(RootSchema);

export const pubsub = new RedisPubSub({
	connection: {
		host: process.env.REDIS_HOST,
		port: process.env.REDIS_PORT,
	},
});

const getGraphQLContext = req => {
	let token;

	// console.debug('...headers', req.headers);

	if ('authorization' in req.headers) {
		token = req.headers.authorization.replace('JWT ', '');
	}

	return {
		token,
		hostname: req.hostname,
	};
};

/**
 * Set up the graphQL HTTP endpoint
 * @param  {Object} app 	express app instance
 */
const setupGraphql = async app => {
	let schema = RootSchema;

	// composed schema
	const composedSchema = schemaBuilder.composeSchema(
		schemaBuilder.composedSchema.composedQueries,
		schemaBuilder.composedSchema.composedMutations
	);
	schema = mergeSchemas({
		schemas: [schema, composedSchema],
	});

	const remoteSchemaList = [
		{
			namespace: 'TEXT',
			url:
				process.env.CHS_TEXTSERVER_URL || 'http://textserver.orphe.us/graphql',
		},
	];

	// conditional schema stitching
	if (process.env.NODE_ENV !== 'test') {
		const validRemoteSchemaList = await validateRemoteSchemas(remoteSchemaList);
		if (validRemoteSchemaList.length > 0) {
			schema = mergeSchemas({
				schemas: [schema, ...validRemoteSchemaList],
			});
		}
	}

	// no null validation rules
	const validationRules = [
		context => {
			context._ast.definitions.forEach(definition => {
				definition.variableDefinitions = definition.variableDefinitions || [];
			});
			return true;
		},
	];

	// final schema
	app.use(
		'/graphql',
		jwtAuthenticate,
		graphqlExpress(req => ({
			schema,
			context: getGraphQLContext(req),
			formatError,
			validationRules,
		}))
	);

	app.use(
		'/graphiql',
		graphiqlExpress({
			endpointURL: '/graphql',
			// subscriptionsEndpoint: `ws://${process.env.WS_SERVER_HOST}:${process.env.WS_SERVER_PORT}/${process.env.WS_SERVER_URI}`
		})
	);

	/**
	 * Websockets server
	 */
	/*
	// Wrap the Express server
	const ws = createServer(app);
	ws.listen(process.env.WS_SERVER_PORT, () => {
		console.log(`GraphQL WebSocket Server is now running on ws://${process.env.WS_SERVER_HOST}:${process.env.WS_SERVER_PORT}`);
		// Set up the WebSocket for handling GraphQL subscriptions
		const subscriptionsServer = new SubscriptionServer({
			execute,
			subscribe,
			schema: RootSchema,
		}, {
			server: ws,
			path: `/${process.env.WS_SERVER_URI}`,
		});
	});
	*/
};

export default setupGraphql;
