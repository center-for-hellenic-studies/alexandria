/**
 * get a list of color meta objects from vision ai image properties
 * 
 */
import getColorCodeRGB from './getColorCodeRGB';
import getColorNames from './getColorNames';

const generateColorMeta = (colors) => {
	colors.map((c) => {
		c.RGB = getColorCodeRGB(c.color);
		c.name = getColorNames(c.RGB);
		return c;
	});
	return colors;
};

export default generateColorMeta;
