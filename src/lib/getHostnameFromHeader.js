import httpContext from 'express-http-context'; // for accessing jwt token

const _parseHostnameFromHeaderHost = headerHost => {
	if (!headerHost) {
		return null;
	}
	return headerHost.split('.')[0];
};

const getHostnameFromHeader = () =>
	_parseHostnameFromHeaderHost(httpContext.get('reqHost'));

export default getHostnameFromHeader;
