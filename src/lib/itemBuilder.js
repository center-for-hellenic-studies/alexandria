import shortid from 'shortid';
import axios from 'axios';
import { GraphQLClient } from 'graphql-request';


/**
 * this does:
 * 	auth to orpheus
 * 	generate item, meta, files
 * 	submit to orpheus project
 */
export default class ItemBuilder {

	/**
	 * construct with user credentials and init auth
	 * @param {*} user 
	 */
	constructor(user) {
		this.user = user;
		this.authorization = null;
		this.client = null;
		console.info(`Orpheus Item Builder initialized for User ${this.user.username}`);
	}

	/**
	 * authorize with user credentials to Orpheus
	 */
	async _authorize() {

		// cache
		if (!this.authorization) {
			// setup
			const {username} = this.user;
			const {password} = this.user;
			const authURL = `${process.env.ORPHEUS_API || 'http://api-staging.orphe.us'}/auth/login`;

			// auth
			console.info(`Authorizing at ${authURL} with Orpheus admin account ${username} ...`);
			const authResult = await axios.post(authURL, {
				username,
				password,
			}).catch((e) => {
				console.error('Authorization failed.', e);
			});

			// success
			console.info('Authorized.');
			this.authorization = authResult.data.token;
		}

		// cache
		if (!this.client) {
			// initiate graphql client
			const orpheusAPIURL = `${process.env.ORPHEUS_API || 'http://api-staging.orphe.us'}/graphql`;
			const client = new GraphQLClient(orpheusAPIURL, {
				headers: {
					Authorization: this.authorization,
				},
			});
			this.client = client;
			console.info(`GraphQL client initialized to ... ${orpheusAPIURL}`);
		}

	}


	/**
	 * process one item with its related files
	 * @param {*} item 
	 * @param {*} files 
	 */
	async processItem(item, files) {

		// define mutation for creating item and files
		const query = `
			mutation itemCreate(
				$hostname: String!, 
				$item: ItemInputType!,
				$files: [FileInputType]
			) {
				itemCreate(
					hostname: $hostname, 
					item: $item,
					files: $files
				) {
					_id
					title
					description
					generatedBy
					metadata {
						type
						value
						label
					}
				}
			}
		`;

		const variables = {
			hostname: this.user.hostname,
			item: item,
			files: files,
		};

		// submit item via graphql mutation
		const itemSubmitted = await this.submitToOrpheus(query, variables);
		console.info('Item submitted ... ', itemSubmitted);

	}

	/**
	 * update one item with its related files
	 * @param {*} item 
	 * @param {*} files 
	 */
	async updateItem(item, files) {

		// define mutation for creating item and files
		const query = `
			mutation itemUpdate(
				$item: ItemInputType!,
				$files: [FileInputType]
			) {
				itemUpdate(
					item: $item,
					files: $files
				) {
					_id
				}
			}
		`;

		const variables = {
			item: item,
			files: files,
		};

		// submit mutation
		const itemSubmitted = await this.submitToOrpheus(query, variables);
		console.info('Item submitted ... ', itemSubmitted);

	}


	// compose OrpheusItem 
	generateOrpheusItem(file) {
		const orpheusItem = {
			_id: shortid.generate(),
			title: `${file.title}`,
			tags: [],
			metadata: []
		};
		return orpheusItem;
	}


	// compose OrpheusFile with associated OrpheusItem
	generateOrpheusFile(fileUploaded, orpheusItem, projectID) {

		if (!fileUploaded) {
			throw new Error('Missing FileUploaded to S3');
		}

		if (!orpheusItem) {
			throw new Error('Missing OrpheusItem associated to this OrpheusFile');
		}

		if (!projectID) {
			throw new Error('Missing Orpheus ProjectID');
		}

		const orpheusFile = {
			_id: fileUploaded.fileID,
			title: fileUploaded.title,
			type: fileUploaded.type,
			projectId: projectID,
			itemId: orpheusItem._id,
		};
		// generate iiif path and thumb
		orpheusFile.name = fileUploaded.s3key;
		orpheusFile.path = `https://iiif.orphe.us/${fileUploaded.s3key}`;
		orpheusFile.thumbPath = `https://iiif.orphe.us/${fileUploaded.s3key}/full/90,/0/default.jpg`;

		return orpheusFile;
	}


	/**
	 * submit Item and Files to Orpheus
	 * @param {*} query 
	 * @param {*} variables 
	 */
	async submitToOrpheus(query, variables) {

		// authorization with admin account
		await this._authorize();

		// submit mutation to orpheus-api
		const gqlResult = await this.client.request(query, variables).catch((err) => {
			console.error('Submit item with error ... ', err);
		});

		return gqlResult;
	}

}
