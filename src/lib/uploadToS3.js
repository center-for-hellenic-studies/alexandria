import AWS from 'aws-sdk';
import request from 'request-promise-native';
import { slugify } from 'underscore.string';
import Path from 'path';
import fs from 'fs';
import shortid from 'shortid';

// config AWS
AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});


/**
 * download a file from URI to a variable in-memory, then upload to s3
 * @param {*} fileName
 * @param {*} fileURI
 */
const downloadAndUploadFileToS3 = async (fileName, fileURI) => {

	if (!fileName || !fileURI) {
		throw new Error('Missing fileURI or fileName to be uploaded to S3');
	}

	// download file to variable in-memory
	const fileBody = await request({
		uri: fileURI,
		encoding: null,
	});

	// unique ID for this file
	const fileUID = shortid.generate();

	// upload file to s3
	const s3 = new AWS.S3();
	const s3Key = `${fileUID}-${slugify(Path.parse(fileName).name)}${Path.parse(fileName).ext}`;
	const fileUploaded = await s3.upload({ // eslint-disable-line
		Bucket: process.env.AWS_BUCKET,
		Key: s3Key,
		Body: fileBody, // file content from variable in-memory
		ACL: 'public-read'
	}).promise();

	// add orpheus unique file ID
	fileUploaded.orpheusFileID = fileUID;

	return fileUploaded;
};

// upload media file to s3
const uploadLocalFileToS3 = async (filePath) => {
	if (!filePath) {
		throw new Error('Missing filePath to be uploaded to S3');
	}
	
	const fileData = fs.readFileSync(filePath);
	console.log(fileData);

	const fileUID = shortid.generate();
	const filename = Path.basename(filePath);
	const [name, ext = 'img'] = filename.split('.');
	const s3key = `${fileUID}-${slugify(name)}.${ext}`;

	const s3 = new AWS.S3();
	const up = await s3.upload({ // eslint-disable-line
		Bucket: process.env.AWS_BUCKET,
		Key: s3key, 
		Body: new Buffer(fileData, 'binary'), // convert raw file data to base64
		ACL: 'public-read'
	}).promise();

	return {
		fileID: fileUID,
		s3: up,
	};
};

export { uploadLocalFileToS3, downloadAndUploadFileToS3 };
