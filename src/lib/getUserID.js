import jsonwebtoken from 'jsonwebtoken';
import httpContext from 'express-http-context'; // for accessing jwt token

const _getUserIDFromToken = token => {
	if (!token) {
		return null;
	}
	const decoded = jsonwebtoken.decode(token);
	return decoded.userId;
};

const getUserID = () => _getUserIDFromToken(httpContext.get('token'));

export default getUserID;
