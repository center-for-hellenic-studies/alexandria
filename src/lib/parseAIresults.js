/**
 * parse AI results into tags
 * 
 */
import _ from 'lodash';

const tagPrefixMapper = {
	aws: 'Rekognition',
	clarifai: 'Clarifai',
	googlevision: 'GCV',
	imagga: 'Imagga',
	microsoftvision: 'MCV',
};


const parseAWSResults = awsLabels => awsLabels.Labels.map(label => label.Name);
const parseClarifaiResults = clarifaiOutputs => clarifaiOutputs[0].data.concepts.map(label => label.name);
const parseGCVResults = GCVresponses => GCVresponses[0].labelAnnotations.map(label => label.description);
const parseImaggaResults = imaggaTags => imaggaTags.result.tags.map(label => label.tag.en);
const parseMCVResults = MCVTags => MCVTags.tags.map(label => label.name);


const parseAIresults = (AIresultsData) => {
	let tags = [];
	let tagsPrefixed = [];
	console.log('...5', AIresultsData.aws, AIresultsData.clarifai, AIresultsData.googlevision, AIresultsData.imagga, AIresultsData.microsoftvision);

	if (AIresultsData.aws.labels) {
		const awsTags = parseAWSResults(AIresultsData.aws.labels);
		tags = _.union(tags, awsTags);
		tagsPrefixed = _.union(tagsPrefixed, awsTags.map(tag => `${tagPrefixMapper.aws}_${tag}`));
	}

	if (AIresultsData.clarifai.outputs.length > 0) {
		const clarifaiTags = parseClarifaiResults(AIresultsData.clarifai.outputs);
		tags = _.union(tags, clarifaiTags);
		tagsPrefixed = _.union(tagsPrefixed, clarifaiTags.map(tag => `${tagPrefixMapper.clarifai}_${tag}`));
	}

	if (AIresultsData.googlevision.responses) {
		const GCVTags = parseGCVResults(AIresultsData.googlevision.responses);
		tags = _.union(tags, GCVTags);
		tagsPrefixed = _.union(tagsPrefixed, GCVTags.map(tag => `${tagPrefixMapper.googlevision}_${tag}`));
	}

	if (AIresultsData.imagga.tags) {
		const imaggaTags = parseImaggaResults(AIresultsData.imagga.tags);
		tags = _.union(tags, imaggaTags);
		tagsPrefixed = _.union(tagsPrefixed, imaggaTags.map(tag => `${tagPrefixMapper.imagga}_${tag}`));
	}

	if (AIresultsData.microsoftvision.analyze) {
		const MCVTags = parseMCVResults(AIresultsData.microsoftvision.analyze);
		tags = _.union(tags, MCVTags);
		tagsPrefixed = _.union(tagsPrefixed, MCVTags.map(tag => `${tagPrefixMapper.microsoftvision}_${tag}`));
	}

	tags = tags.map(tag => _.lowerCase(tag));
	tags = _.union(tags, tagsPrefixed);

	return tags;
};

export default parseAIresults;
