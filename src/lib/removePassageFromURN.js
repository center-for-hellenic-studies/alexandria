const _isPassage = passage => {
	const passageList = passage.split('-');
	const matchedNonNumber = passageList.filter(
		passage => !passage.match(/^\d*\.?\d*$/g)
	);
	return !matchedNonNumber.length;
};

const removePassageFromURN = urn => {
	const passage = urn.split(':').pop();
	if (_isPassage(passage)) {
		const urnAsList = urn.split(':');
		urnAsList.pop();
		const urnWithoutPassage = urnAsList.join(':');
		return urnWithoutPassage;
	}
	return urn;
};

export default removePassageFromURN;
