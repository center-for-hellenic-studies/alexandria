// transform the values passed from graphql to args for a mongo Items query
const itemFiltersToMongoQueryArgs = ({
	projectId, collectionId, textsearch, filter, dateSearch, tags, ids, offset, limit 
}) => {
	const args = {};

	if (!projectId && !collectionId) {
		return [];
	}

	if (projectId) {
		args.projectId = projectId;
	}

	if (collectionId) {
		args.collectionId = collectionId;
	}

	if (ids) {
		args._id = ids;
	}

	if (textsearch) {
		args.$text = { $search: textsearch };
	}

	if (filter) {
		args.$and = [];
		filter.forEach((filterParam) => {
			args.$and.push({
				metadata: {
					$elemMatch: {
						label: filterParam.name,
						value: { $in: filterParam.values },
					},
				},
			});
		});
	}

	// build filter for array of date ranges
	if (dateSearch) {
		// init mongo and structure
		if (!args.$and) {
			args.$and = [];
		}
		// build each date range filter
		dateSearch.map((dateInput) => {
			// set date range
			const dateRange = {};
			if (dateInput.dateStart) {
				dateRange.$gte = dateInput.dateStart;
			}
			if (dateInput.dateEnd) {
				dateRange.$lte = dateInput.dateEnd;
			}
			// build mongo filter
			args.$and.push({
				metadata: {
					$elemMatch: {
						label: dateInput.dateFieldLabel,
						value: { ...dateRange },
					},
				},
			});
		});
	}

	if (tags) {
		args.tags = { $in: tags };
	}

	return args;
};


export default itemFiltersToMongoQueryArgs;
