/**
 * fetch AI results from data-grinder-api
 * 
 */
import axios from 'axios';

const getAIresults = async imageURL => await axios.post(
	`${process.env.DATA_GRINDER}/json`,
	{
		url: imageURL,
	}
);

export default getAIresults;
