import Project from '../models/project';
import getHostnameFromHeader from './getHostnameFromHeader';

const getCurrentCommentaryID = async () => {
	const hostname = getHostnameFromHeader();
	if (hostname) {
		const commentary = await Project.findOne({ hostname });
		if (commentary) {
			return commentary._id;
		}
	}
	return null;
};

export default getCurrentCommentaryID;
