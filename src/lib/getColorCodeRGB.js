/**
 * get a list of color names from a list of color objects from vision ai
 * 
 */
import colorConvertor from 'color-convert';

const getColorCodeRGB = color => `#${colorConvertor.rgb.hex(color.red, color.green, color.blue)}`;

export default getColorCodeRGB;
