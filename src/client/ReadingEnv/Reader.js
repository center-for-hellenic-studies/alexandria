/**
 * @prettier
 */

import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

const markPages = (target, selector) => {
	const paragraphs = target.querySelectorAll(selector);

	paragraphs.forEach((p, i, ps) => {
		if (i === 0) {
			p.dataset.previousPageBreak = true;
			return;
		}

		delete p.dataset.nextPageBreak;
		delete p.dataset.previousPageBreak;

		const next = ps[i + 1] || ps[ps.length - 1];
		const prev = ps[i - 1];
		const rects = Array.from(p.getClientRects());
		const nextRects = next.getClientRects();
		const prevRects = prev.getClientRects();
		const { x: nextX } = nextRects[nextRects.length - 1];
		const { x: prevX } = prevRects[0];

		if (rects[rects.length - 1].x > prevX) {
			p.dataset.nextPageBreak = true;
		}

		if (rects[0].x < nextX) {
			p.dataset.previousPageBreak = true;
		}
	});
};

const scrollToNextUnseenNode = (target, currentPage) => {
	const pages = target.querySelectorAll('[data-next-page-break]');

	for (let i = 0, l = pages.length; i < l; i++) {
		const p = pages[i];
		const rects = p.getClientRects();
		const { x } = rects[rects.length - 1];

		if (x > target.clientWidth && p.dataset.nextPageBreak) {
			p.scrollIntoView({ block: 'nearest' });

			return p;
		}
	}
};

const scrollToPreviousUnseenNode = (target, currentPage) => {
	const pages = target.querySelectorAll('[data-previous-page-break]');

	for (let i = pages.length - 1; i >= 0; i--) {
		const p = pages[i];
		const rects = p.getClientRects();
		const { x } = rects[0];

		if (x <= 0 && p.dataset.previousPageBreak) {
			p.scrollIntoView({
				block: 'nearest',
			});

			return p;
		}
	}
};

const scrollToUnseenNode = (target, direction) => {
	const singleColumn =
		window.getComputedStyle(target).getPropertyValue('column-count') === '1';

	if (direction === 'next') {
		const scrollBy = singleColumn
			? target.clientWidth + 19
			: target.clientWidth / 2 + 10;

		target.scrollBy(scrollBy, 0);
	} else if (direction === 'previous') {
		const scrollBy = singleColumn
			? -target.clientWidth - 19
			: -target.clientWidth / 2 - 10;

		target.scrollBy(scrollBy, 0);
	}
};

const Reader = ({ selector, text }) => {
	const { content, id } = text;
	const readingEnvRef = useRef();
	const handleClick = e => {
		const { clientX, nativeEvent } = e;
		const { target } = nativeEvent;
		const { nodeName } = target;

		// bail if the target is a link
		if (nodeName.toLowerCase() === 'a') {
			return window.requestAnimationFrame(() => {
				const aim = document.querySelector(
					`a[name='${target.hash.replace('#', '')}']`
				);

				if (aim) {
					nativeEvent.preventDefault();
					nativeEvent.stopPropagation();

					aim.scrollIntoView({ inline: 'start' });
				}
			});
		}

		const container = readingEnvRef.current;
		const direction = clientX > container.clientWidth / 2 ? 'next' : 'previous';

		scrollToUnseenNode(container, direction, selector);
	};

	useEffect(() => markPages(readingEnvRef.current, selector), [id]);
	useEffect(() => {
		const handleResize = () => markPages(readingEnvRef.current, selector);
		readingEnvRef.current.addEventListener('resize', handleResize);
		return () => {
			readingEnvRef.current.removeEventListener('resize', handleResize);
		};
	});
	useEffect(() => {}, ['hot']);

	return (
		<div
			id="@@reading-env/text"
			dangerouslySetInnerHTML={{ __html: content }}
			onClick={handleClick}
			ref={readingEnvRef}
		/>
	);
};

Reader.defaultProps = {
	text: {},
};

Reader.propTypes = {
	text: PropTypes.shape({
		content: PropTypes.string,
		id: PropTypes.string,
		title: PropTypes.string,
	}),
};

export default Reader;
