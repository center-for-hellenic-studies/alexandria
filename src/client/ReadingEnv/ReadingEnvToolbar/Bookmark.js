/**
 * @prettier
 */

class Bookmark {
	QUERY_SELECTOR_PARAGRAPHS = {
		ahcip: '.Paragraph',
		chs: 'p',
	};

	constructor(pages, source) {
		this.pages = pages;
		this.source = source;
		this.querySelectorParagraph = this.QUERY_SELECTOR_PARAGRAPHS[source];
		this.el = document.getElementById('@@reading-env/bookmark');
		this.listener = this.el.addEventListener('click', this.handleClick);
	}

	handleClick = (e) => {
		e.preventDefault();

		const { currentPage } = this.pages;
		const pageEl = document.getElementById(`page-${currentPage}`);
		const targetParagraph = pageEl.querySelector(this.querySelectorParagraph);
		const paragraphs = this.pages.container
			.querySelectorAll(this.querySelectorParagraph)
			.entries();

		let targetParagraphIndex = 0;
		let done = false;
		while (!done) {
			const entry = paragraphs.next();
			const [index, node] = entry.value;
			done = entry.done;

			if (node === targetParagraph) {
				targetParagraphIndex = index;

				break;
			}
		}

		this.pages.addBookmark(targetParagraphIndex);
	};
}

export default Bookmark;
