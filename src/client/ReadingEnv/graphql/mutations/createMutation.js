/**
 * @prettier
 */

import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';
const commentCreate = gql`
	mutation commentCreate($hostname: String!, $comment: CommentInputType!) {
		commentCreate(comment: $comment, hostname: $hostname) {
			_id
		}
	}
`;

const commentCreateMutation = graphql(commentCreate, {
	props: params => ({
		commentCreate: comment =>
			params.commentCreateMutation({
				variables: {
					hostname: getCurrentArchiveHostname(),
					comment,
				},
			}),
	}),
	name: 'commentCreateMutation',
	options: {
		refetchQueries: ['projectCommentsQuery'],
	},
});

export default commentCreateMutation;
