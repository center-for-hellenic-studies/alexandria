/**
 * @prettier
 */

import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
const commentUpdate = gql`
	mutation commentUpdate($comment: CommentInputType!) {
		commentUpdate(comment: $comment) {
			_id
		}
	}
`;

const commentUpdateMutation = graphql(commentUpdate, {
	props: params => ({
		commentUpdate: comment => {
			// https://github.com/apollographql/apollo-feature-requests/issues/6
			// Apollo just adds these __typename declarations to the queried
			// object, but it will reject them if they're present in the
			// mutation
			delete comment.__typename;
			delete comment.lemmaCitation.__typename;

			return params.commentUpdateMutation({
				variables: {
					comment,
				},
			});
		},
	}),
	name: 'commentUpdateMutation',
	options: {
		refetchQueries: ['projectCommentsQuery'],
	},
});

export default commentUpdateMutation;
