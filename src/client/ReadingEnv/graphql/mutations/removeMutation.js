/**
 * @prettier
 */

import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';

const commentRemove = gql`
	mutation commentRemove($hostname: String!, $_id: String!) {
		commentRemove(_id: $_id, hostname: $hostname) {
			_id
		}
	}
`;

const commentRemoveMutation = graphql(commentRemove, {
	props: params => ({
		commentRemove: _id => {
			return params.commentRemoveMutation({
				variables: {
					hostname: getCurrentArchiveHostname(),
					_id,
				},
			});
		},
	}),
	name: 'commentRemoveMutation',
	options: {
		refetchQueries: ['projectCommentsQuery'],
	},
});

export default commentRemoveMutation;
