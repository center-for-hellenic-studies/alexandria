import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';

const query = gql`
	query textQuery($id: Int) {
		TEXT_work(id: $id) {
			id
			slug
			english_title
			original_title
			form
			work_type
			label
			description
			language {
				id
				title
				slug
			}
			version {
				id
				slug
				title
				urn
				description
			}
			exemplar {
				id
				slug
				title
				description
			}
			translation {
				id
				slug
				title
				description
			}
			textNodes {
				id
				location
				text
			}
		}
	}
`;

const textQuery = graphql(query, {
	name: 'textQuery',
	options: props => {
		let id;

		if (props.match && props.match.params) {
			id = parseInt(props.match.params.id, 10);
		}

		return {
			variables: {
				id,
			},
		};
	},
});

export default textQuery;
