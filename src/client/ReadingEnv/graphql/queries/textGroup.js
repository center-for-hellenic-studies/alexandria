import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';

import collections from '../../../texts/graphql/texts';

function lookupTextGroupById(id) {
	let textsLookup = {};
	collections.forEach(collection => {
		collection.textGroups.forEach(textGroup => {
			textGroup.works.forEach(work => {
				work.textGroupId = textGroup.id;
				textsLookup[work.id] = work;
			});
		});
	});
	console.log('work id: ', id);
	console.log('text groupd id: ', textsLookup[id].textGroupId);
	return textsLookup[id].textGroupId;
}

const query = gql`
	query texGrouptQuery($id: Int) {
		TEXT_textGroup(id: $id) {
			id
			title
			works {
				id
				slug
				english_title
				original_title
				form
				work_type
				label
				description
				language {
					id
					title
					slug
				}
				version {
					id
					slug
					title
					urn
					description
				}
				exemplar {
					id
					slug
					title
					description
				}
				translation {
					id
					slug
					title
					description
				}
			}
		}
	}
`;

const textGroupQuery = graphql(query, {
	name: 'textGroupQuery',
	options: props => {
		let id;
		let work_id;

		if (props.match && props.match.params) {
			work_id = parseInt(props.match.params.id, 10);
		}
		id = lookupTextGroupById(work_id);
		console.log(':::::ID:::: ', id);

		return {
			variables: {
				id,
			},
		};
	},
});

export default textGroupQuery;
