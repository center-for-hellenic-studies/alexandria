/**
 * @prettier
 */

import React from 'react';
import { Bookmark, FindInPage, Menu } from '@material-ui/icons';

const _addBookmark = selector => async e => {
	e.preventDefault();

	const els = document.querySelectorAll(selector);

	for (let i = 0, l = els.length; i < l; i++) {
		const el = els[i];
		const { x } = el.getBoundingClientRect();

		// get the first visible el
		if (x >= 0) {
			const options = {
				body: JSON.stringify({ bookmark: { paragraphIndex: i } }),
				cache: 'no-cache',
				headers: {
					'Content-Type': 'application/json',
				},
				method: 'POST',
			};
			const url = `${window.location.pathname}/bookmarks`;
			const response = await fetch(url, options);
			const json = await response.json();
			return console.log(json);
		}
	}
};

const toggleMenu = () => {};

const Header = ({ selector }) => {
	const addBookmark = _addBookmark(selector);

	return (
		<header>
			<nav className="flex space-between w100">
				<ul className="flex-start">
					<li>
						<a href="//alexandria.staging.archimedes.digital">New Alexandria</a>
					</li>
					{/*
					<li>
						<a href="/read/chs/6199">CHS Reading Environment</a>
					</li>
					<li>
						<a href="/read/textserver/urn:cts:greekLit:tlg0548.tlg001.perseus-grc2">
							CHS Textserver
						</a>
					</li>
					*/}
				</ul>
				<ul>
					<li>
						<a
							className="workTitle"
							href="//read/chs/urn:cts:greekLit:tlg0548.tlg001.perseus-grc2"
						>
							The Iliad
						</a>
					</li>
				</ul>
				<ul className="flex-end">
					<li>
						<button onClick={toggleMenu}>
							<Menu />
						</button>
					</li>
					<li>
						<button>
							<FindInPage />
						</button>
					</li>
					<li>
						<button role="button" className="pointer" onClick={addBookmark}>
							<Bookmark />
						</button>
					</li>
				</ul>
			</nav>
		</header>
	);
};

export default Header;
