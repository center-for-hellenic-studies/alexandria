/**
 * @prettier
 */

import React, { useEffect, useRef, useState } from 'react';

const toggleSidePanel = () => {
	document.body.classList.toggle('sidePanelVisible');
};

const SidePanel = ({ panelVisible, togglePanelVisible }) => {
	return (
		<div className="sidePanelOuter">
			<div className="sidePanelHandle" onClick={toggleSidePanel}>
				<div className="sidePanelHandleBar" />
			</div>
			<div className="sidePanel">
				<div className="sidePanelHeader">
					<select>
						<optgroup label="Commentaries">
							<option value="commentary">A Homer Commentary in Progress</option>
						</optgroup>
						<optgroup label="Editions">
							<option>Venetus A</option>
							<option>Escorial Upsilon</option>
							<option>
								1920, David B. Munro, Thomas W. Allen, Oxonii, e typographeo
								Clarendoniano
							</option>
						</optgroup>
						<optgroup label="Translations">
							<option>The Loeb Classical Library</option>
							<option>The Center for Hellenic Studies Translation</option>
						</optgroup>
					</select>
				</div>
				<div className="sidePanelContent">
					<div className="comment">
						<h2>Iliad 1</h2>
						<span className="commentByline">Gregory Nagy, 20 Aug 2018</span>
						<div className="commentBody">
							<p>
								The comments I offered in Classical Inquiries 2016-2017 on Iliad
								Rhapsody 1 through Rhapsody 24, starting here with Rhapsody 1,
								were based mostly on details that derive from seven books that I
								indicate in the Bibliography by way of these abbreviations: BA,
								GMP, H24H, HC, HPC, HQ, HR, MoM, PasP, PH. Each one of these
								books has its own index locorum. My colleague Anita Nikkanen, an
								Associate Editor for the online project A Homer commentary in
								progress, tracked the sequences of Homeric verses as listed in
								the indices for six of these books and then summarized my
								comments on those verses. Following up on her meticulous work, I
								am in the process of converting her summaries into a form of
								commentary that is being incorporated into AHCIP. My comments on
								the Iliad as I presented them in Classical Inquiries 2016-2017
								are merely samplings of the content that I hope to contribute to
								the overall commentary, to which a number of other colleagues
								are also contributing their own comments. That said, I now
								proceed to offer a sampling of comments on Rhapsody 1. At this
								point, my comments about this beginning of beginnings need no
								further introduction of their own.
							</p>
						</div>
					</div>
					<div className="comment">
						<h2>Iliad 1.1-12</h2>
						<span className="commentByline">Gregory Nagy, 10 Sept 2018</span>
						<div className="commentBody">
							<p>
								The main theme of the narration is signaled right away. By theme
								I mean a basic unit of content or meaning in Homeric poetry: see
								the inventory of Words and Ideas. The signaling is accomplished
								by way of the first word of the very first verse of the Homeric
								Iliad. The word is mēnis ‘anger’, I.01.001, and it refers to the
								anger of Achilles. A definitive book on this word is Muellner
								1996. The Master Narrator begins his narration by focusing on
								this anger: he invokes a Muse, as a goddess of inspiration whom
								he addresses here simply as theā ‘goddess’, and he calls on her
								to sing for him this anger, I.01.001. On the term Master
								Narrator, see the inventory of Words and Ideas; on the idea of
								the Muse(s) as the goddess(es) of poetic inspiration, see the
								general comment at I.02.484-487 and the special comments at
								I.02.484 and at I.02.761. The subject that the Master Narrator
								has chosen to narrate, the anger of Achilles, is the grammatical
								object of the verb aeidein ‘sing’. So, the narrative subject is
								the grammatical object. The Master Narrator is calling on the
								Muse to sing the anger, not just sing about the anger. The song
								is not only about the anger: it is the anger itself. The song
								captures the total reality of the anger. The Master Narrator
								proceeds to tell about this anger: it happened because of a
								quarrel, as signaled especially by the words erizein ‘have
								strife’ at I.01.006 and eris ‘strife’ at I.01.008. This quarrel
								in the Iliad is parallel to another quarrel that is narrated in
								a “micro-Iliad” that we find embedded in the Odyssey. This
								micro-Iliad is the First Song of Demodokos, O.08.072–083, and
								the quarrel there is signaled especially by the word neikos
								‘quarrel’ at O.08.075. It has been debated whether the quarrel
								scene in this “micro-Iliad” was modeled on a quarrel-scene in
								the Cypria, which was part of the epic Cycle. On the terms epic
								and epic Cycle, see the Inventory of terms and names. But the
								quarrel scenes of the Iliad, the Cypria, and the “micro-Iliad”
								can be seen as stemming from epic traditions that were
								originally independent of each other. On the term epic, used
								here for the first time in these comments on the Homeric Iliad,
								see again the inventory of Words and Ideas.
							</p>
						</div>
					</div>
					<div className="comment">
						<h2>Iliad 1.1-2</h2>
						<span className="commentByline">Gregory Nagy, 4 Sept 2018</span>
						<div className="commentBody">
							<p>
								The mēnis of Achilles is a special kind of ‘anger’. The hero
								feels this anger after his tīmē ‘honor’ is damaged by the
								over-king Agamemnon. The Master Narrator says at verse 2 that
								this special anger caused algea ‘pains’ for the Achaeans. The
								name Akhaioi ‘Achaeans’ here at verse 2 refers to the heroes who
								were viewed by the ancient Greeks in a later age as their
								prototypes in an earlier age of heroes; two synonyms for
								‘Achaeans’ in the Iliad are Danaoi ‘Danaans’ and Argeioi
								‘Argives’. The word mēnis at verse 1 does not yet refer to the
								anger of persons other than Achilles in the Iliad. As the
								narration continues, however, it becomes clear that the god
								Apollo felt mēnis ‘anger’ at the Achaeans even before the hero
								Achilles felt his own mēnis.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default SidePanel;
