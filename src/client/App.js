/**
 * @prettier
 */

import { hot } from 'react-hot-loader/root';
import React from 'react';
import Header from './ReadingEnv/Header';
import Reader from './ReadingEnv/Reader';
import SidePanel from './ReadingEnv/SidePanel';

const QUERY_SELECTORS = {
	ahcip: '.Paragraph',
	chs: 'p',
};

const App = React.memo(
	({ text = {} }) => (
		<React.Fragment>
			<Header selector={QUERY_SELECTORS[text.source]} text={text} />
			<Reader selector={QUERY_SELECTORS[text.source]} text={text} />
			<SidePanel selector={QUERY_SELECTORS[text.source]} text={text} />
		</React.Fragment>
	),
	(prevProps, nextProps) => {
		if (prevProps.text && !nextProps.text) {
			return true;
		}
	}
);

export default hot(App);
