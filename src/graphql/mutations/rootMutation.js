import { GraphQLObjectType } from 'graphql';

import projectMutationFields from './projects';
import collectionMutationFields from './collections';
import itemMutationFields from './items';
import userMutationFields from './users';

import commentMutationFields from './comments';

/**
 * Root mutations
 * @type {GraphQLObjectType}
 */
const RootMutations = new GraphQLObjectType({
	name: 'RootMutationType',
	description: 'Root mutation object type',
	fields: {
		...projectMutationFields,
		...collectionMutationFields,
		...itemMutationFields,
		...userMutationFields,
		...commentMutationFields,
	},
});

export default RootMutations;
