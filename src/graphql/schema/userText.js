import authorize from '../../lib/authorize'; 

import UserTextTC from '../typecomposers/userText';

const Queries = {
	userTextMany: UserTextTC.getResolver('findMany'),
};


const Mutations = {
	...authorize('userIDExist', {
		userTextCreate: UserTextTC.getResolver('createOneWithUserID'),
	}),
	...authorize('userOwnsUserText', {
		userTextUpdateByID: UserTextTC.getResolver('updateById'),
		userTextRemoveByID: UserTextTC.getResolver('removeById'),
	}),
};


export {
	Queries,
	Mutations,
};
