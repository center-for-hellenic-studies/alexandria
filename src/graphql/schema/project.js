import authorize from '../../lib/authorize'; 

import ProjectTC from '../typecomposers/project';

const Queries = {
	projectMany: ProjectTC.getResolver('findMany'),
	project: ProjectTC.getResolver('findMany'),

};


const Mutations = {
	...authorize('userIDExist', {
		projectCreate: ProjectTC.getResolver('createAndOwnOne'),
	}),
	...authorize('userOwnsProject', {
		projectUpdateByID: ProjectTC.getResolver('updateById'),
		projectRemoveByID: ProjectTC.getResolver('removeById'),
	}),
};


export {
	Queries,
	Mutations,
};
