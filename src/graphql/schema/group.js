import authorize from '../../lib/authorize'; 

import GroupTC from '../typecomposers/group';

const Queries = {
	groupMany: GroupTC.getResolver('findMany'),
};


const Mutations = {
	// role order matters, check role access from left to right
	...authorize('userIDExist', {
		groupCreate: GroupTC.getResolver('createAndOwnOne'),
	}),
	...authorize('userOwnsGroup', {
		groupUpdateByID: GroupTC.getResolver('updateById'),
		groupRemoveByID: GroupTC.getResolver('removeById'),
	}),
	
	
};


export {
	Queries,
	Mutations,
};
