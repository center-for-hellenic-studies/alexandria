import _s from 'underscore.string';

// services
import PermissionsService from './PermissionsService';

// models
import Item from '../../models/item';
import Project from '../../models/project';

// errors
import { AuthenticationError, PermissionError } from '../errors';

// lib
import itemFiltersToMongoQueryArgs from '../../lib/itemFiltersToMongoQueryArgs';

/**
 * Logic-layer service for dealing with fields
 */

export default class FieldService extends PermissionsService {

	/**
	 * Get a list of fields
	 * @param {string} projectId
	 * @returns {Object[]} array of fields
	 */
	async getFields({
		projectId, collectionId, textsearch, filter, dateSearch, tags, ids, offset, limit 
	}) {
		const args = itemFiltersToMongoQueryArgs({
			projectId, collectionId, textsearch, filter, dateSearch, tags, ids, offset, limit 
		});
		const fields = [];
		const tagValues = [];

		const labels = await Item.distinct('metadata.label', { projectId, });
		labels.forEach((label) => {
			fields.push({
				name: label,
				values: [],
				type: ''
			});
		});

		const items = await Item.find(args);

		items.forEach((item) => {
			item.metadata.forEach((metadatum) => {
				if (['text', 'number', 'date', 'color'].indexOf(metadatum.type) >= 0) {
					fields.forEach((field) => {
						if (field.name === metadatum.label) {
							if (!field.type) {
								field.type = metadatum.type;
							}
							if (field.values.indexOf(metadatum.value) < 0) {
								field.values.push(metadatum.value);
							}
						}
					});
				}
			});
			item.tags.forEach((tag) => {
				if (tagValues.indexOf(tag) < 0) {
					tagValues.push(tag);
				}
			});
		});


		fields.push({
			name: 'Tags',
			values: tagValues,
		});

		return fields;
	}

}
