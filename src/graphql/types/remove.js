import { GraphQLObjectType, GraphQLString } from 'graphql';

export default new GraphQLObjectType({
	name: 'RemoveType',
	fields: {
		result: {
			type: GraphQLString,
		},
	},
});
