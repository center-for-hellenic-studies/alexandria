import {
	GraphQLString, GraphQLList, GraphQLObjectType, GraphQLInputObjectType,
} from 'graphql';

const config = {
	name: 'FieldType',
	description: 'Metadata Field Schema base query type',
	class: 'GraphQLObjectType',
	fields: {
		name: {
			type: GraphQLString,
		},
		values: {
			type: new GraphQLList(GraphQLString),
		},
		type: {
			type: GraphQLString,
		}
	},
};

const inputConfig = {
	name: 'FieldInputType',
	description: 'Metadata Field Schema base query type',
	class: 'GraphQLInputObjectType',
	fields: {
		name: {
			type: GraphQLString,
		},
		values: {
			type: new GraphQLList(GraphQLString),
		},
	},
};

const dateRangeInputConfig = {
	name: 'DateRangeInputType',
	description: 'Date range search in Metadata Fields',
	class: 'GraphQLInputObjectType',
	fields: {
		dateFieldLabel: {
			type: GraphQLString,
		},
		dateStart: {
			type: GraphQLString,
		},
		dateEnd: {
			type: GraphQLString,
		},
	},
};

const FieldType = new GraphQLObjectType(config);
const FieldInputType = new GraphQLInputObjectType(inputConfig);
const DateRangeInputType = new GraphQLInputObjectType(dateRangeInputConfig);

export default FieldType;
export { FieldInputType, DateRangeInputType };
