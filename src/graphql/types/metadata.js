import { GraphQLList, GraphQLID } from 'graphql';
import createType from 'mongoose-schema-to-graphql';
import GraphQLJSON from 'graphql-type-json';

// schema
import { MetadataSchema } from '../../models/item';

const config = {
	name: 'MetadataType',
	description: 'Metadata schema base query type',
	class: 'GraphQLObjectType',
	schema: MetadataSchema,
	exclude: ['_id'],
	extend: {
		valueJSON: { type: GraphQLJSON },
	}
};

const configInput = {
	name: 'MetadataInputType',
	description: 'Metadata schema base input type',
	class: 'GraphQLInputObjectType',
	schema: MetadataSchema,
	exclude: ['_id'],
	extend: {
		valueJSON: { type: GraphQLJSON },
	}
};

const MetadataType = createType(config);
const MetadataInputType = createType(configInput);

export default MetadataType;
export { MetadataInputType };
