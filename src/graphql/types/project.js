import {
	GraphQLObjectType,
	GraphQLList,
	GraphQLID,
	GraphQLString,
	GraphQLInt,
	GraphQLBoolean,
	GraphQLNonNull,
} from 'graphql';
import createType from 'mongoose-schema-to-graphql';

// models
import Project from '../../models/project';

// logic
import CollectionService from '../logic/collections';
import FileService from '../logic/files';
import ItemService from '../logic/items';
import ProjectService from '../logic/projects';
import UserService from '../logic/users';
import PageService from '../logic/pages';
import TagService from '../logic/tags';
import FieldService from '../logic/fields';

import CommentService from '../logic/comments/comments';

// types
import CollectionType from './collection';
import FileType from './file';
import ItemType from './item';
import ActivityItemType from './activityItem';
import UserType from './user';
import PageType from './page';
import FieldType, { FieldInputType, DateRangeInputType } from './field';

import CommentType from '../types/comment';

const config = {
	name: 'ProjectType',
	description: 'Project base query type',
	class: 'GraphQLObjectType',
	schema: Project.schema,
	extend: {
		comments: {
			type: new GraphQLList(CommentType),
			description: 'Get list of all comments in this project',
			args: {
				queryParam: {
					type: GraphQLString,
				},
				limit: {
					type: GraphQLInt,
				},
				skip: {
					type: GraphQLInt,
				},
				sortRecent: {
					type: GraphQLBoolean,
				},
			},
			async resolve(
				parent,
				{ queryParam, limit, skip, sortRecent },
				{ token }
			) {
				const commentService = new CommentService(token);
				const comments = commentService.commentsGet(
					queryParam,
					limit,
					skip,
					sortRecent,
					parent._id
				);
				return comments;
			},
		},
		commentsOn: {
			type: new GraphQLList(CommentType),
			description:
				'Get list of comments via urn and paginated via skip/limit. Relates a scholion to the passage of text it comments on.',
			args: {
				urn: {
					type: new GraphQLNonNull(GraphQLString),
				},
				limit: {
					type: GraphQLInt,
				},
				skip: {
					type: GraphQLInt,
				},
			},
			async resolve(parent, { urn, limit, skip }, { token }) {
				const commentService = new CommentService(token);
				const comments = await commentService.commentsGetByURN(
					urn,
					limit,
					skip
				);
				return comments;
			},
		},
		collection: {
			type: CollectionType,
			description: 'Get collection document',
			args: {
				_id: {
					type: GraphQLString,
				},
				slug: {
					type: GraphQLString,
				},
				hostname: {
					type: GraphQLString,
				},
			},
			resolve(parent, { _id, slug, hostname }, { token }) {
				const collectionService = new CollectionService(token);
				return collectionService.getCollection({
					projectId: parent._id,
					_id,
					slug,
					hostname,
				});
			},
		},
		collections: {
			type: new GraphQLList(CollectionType),
			description: 'Get list of collections',
			args: {
				textsearch: {
					type: GraphQLString,
				},
				limit: {
					type: GraphQLInt,
				},
				offset: {
					type: GraphQLInt,
				},
			},
			resolve(parent, { textsearch, limit, offset }, { token }) {
				const collectionService = new CollectionService(token);
				return collectionService.getCollections({
					projectId: parent._id,
					textsearch,
					limit,
					offset,
				});
			},
		},
		collectionsCount: {
			type: GraphQLInt,
			description: 'Get count of collection for project',
			resolve(parent, _, { token }) {
				const collectionService = new CollectionService(token);
				return collectionService.count({ projectId: parent._id });
			},
		},
		page: {
			type: PageType,
			description: 'Get page document',
			args: {
				_id: {
					type: GraphQLString,
				},
				slug: {
					type: GraphQLString,
				},
				hostname: {
					type: GraphQLString,
				},
			},
			resolve(parent, { _id, slug, hostname }, { token }) {
				const pageService = new PageService(token);
				return pageService.getPage({
					projectId: parent._id,
					_id,
					slug,
					hostname,
				});
			},
		},
		pages: {
			type: new GraphQLList(PageType),
			description: 'Get list of pages',
			args: {
				textsearch: {
					type: GraphQLString,
				},
				limit: {
					type: GraphQLInt,
				},
				offset: {
					type: GraphQLInt,
				},
			},
			resolve(parent, { textsearch, limit, offset }, { token }) {
				const pageService = new PageService(token);
				return pageService.getPages({
					projectId: parent._id,
					textsearch,
					limit,
					offset,
				});
			},
		},
		pagesCount: {
			type: GraphQLInt,
			description: 'Get count of pages in project',
			resolve(parent, _, { token }) {
				const pageService = new PageService(token);
				return pageService.count({ projectId: parent._id });
			},
		},
		activity: {
			type: new GraphQLList(ActivityItemType),
			description: 'Get project activity',
			resolve(parent, { limit, offset }, { token }) {
				const projectService = new ProjectService(token);
				return projectService.getActivityFeed({
					projectId: parent._id,
					limit,
					offset,
				});
			},
		},
		users: {
			type: new GraphQLList(
				new GraphQLObjectType({
					name: 'ProjectUsersType',
					fields: {
						user: {
							type: UserType,
							resolve(projectUser, args, { token }) {
								const userService = new UserService(token);
								return userService.getUser({ _id: projectUser.userId });
							},
						},
						role: {
							type: GraphQLString,
						},
					},
				})
			),
			resolve(parent, args, { token }) {
				return parent.users;
			},
		},
		userIsAdmin: {
			type: GraphQLBoolean,
			resolve(parent, args, { token }) {
				const userService = new UserService(token);
				return userService.userIsAdmin({ project: parent });
			},
		},
		item: {
			type: ItemType,
			description: 'Get item document',
			args: {
				_id: {
					type: GraphQLString,
				},
				slug: {
					type: GraphQLString,
				},
			},
			resolve(parent, { _id, slug }, { token }) {
				const itemService = new ItemService(token);
				return itemService.getItem({ projectId: parent._id, _id, slug });
			},
		},
		items: {
			type: new GraphQLList(ItemType),
			description: 'Get list of items',
			args: {
				textsearch: {
					type: GraphQLString,
				},
				filter: {
					type: new GraphQLList(FieldInputType),
				},
				dateSearch: {
					type: new GraphQLList(DateRangeInputType),
				},
				tags: {
					type: new GraphQLList(GraphQLString),
				},
				ids: {
					type: new GraphQLList(GraphQLString),
				},
				limit: {
					type: GraphQLInt,
				},
				offset: {
					type: GraphQLInt,
				},
			},
			resolve(
				parent,
				{ textsearch, filter, dateSearch, tags, ids, limit, offset },
				{ token }
			) {
				const itemService = new ItemService(token);
				return itemService.getItems({
					projectId: parent._id,
					textsearch,
					filter,
					dateSearch,
					tags,
					ids,
					limit,
					offset,
				});
			},
		},
		itemsCount: {
			type: GraphQLInt,
			description: 'Get count of items in project',
			args: {
				textsearch: {
					type: GraphQLString,
				},
				filter: {
					type: new GraphQLList(FieldInputType),
				},
				tags: {
					type: new GraphQLList(GraphQLString),
				},
			},
			resolve(parent, { textsearch, filter, tags }, { token }) {
				const itemService = new ItemService(token);
				return itemService.count({
					projectId: parent._id,
					textsearch,
					filter,
					tags,
				});
			},
		},
		files: {
			type: new GraphQLList(FileType),
			description: 'Get project files',
			args: {
				limit: {
					type: GraphQLInt,
				},
			},
			resolve(parent, { limit }, { token }) {
				const fileService = new FileService(token);
				return fileService.getFiles({ projectId: parent._id, limit });
			},
		},
		file: {
			type: FileType,
			description: 'Get a project file',
			args: {
				_id: {
					type: GraphQLString,
				},
			},
			resolve(parent, { _id }, { token }) {
				const fileService = new FileService(token);
				return fileService.getFile({ projectId: parent._id, _id });
			},
		},
		filesCount: {
			type: GraphQLInt,
			description: 'Count all project files',
			resolve(parent, args, { token }) {
				const fileService = new FileService(token);
				return fileService.count({ projectId: parent._id });
			},
		},
		metaFields: {
			type: new GraphQLList(FieldType),
			description: 'Get unique meta fields',
			args: {
				textsearch: {
					type: GraphQLString,
				},
				filter: {
					type: new GraphQLList(FieldInputType),
				},
				dateSearch: {
					type: new GraphQLList(DateRangeInputType),
				},
				tags: {
					type: new GraphQLList(GraphQLString),
				},
				ids: {
					type: new GraphQLList(GraphQLString),
				},
				limit: {
					type: GraphQLInt,
				},
				offset: {
					type: GraphQLInt,
				},
			},
			resolve(
				parent,
				{ textsearch, filter, dateSearch, tags, ids, limit, offset },
				{ token }
			) {
				const fieldService = new FieldService(token);
				return fieldService.getFields({
					projectId: parent._id,
					textsearch,
					filter,
					dateSearch,
					tags,
					ids,
					limit,
					offset,
				});
			},
		},
		tags: {
			type: new GraphQLList(GraphQLString),
			description: 'Get project tags',
			resolve(parent, args, { token }) {
				const tagService = new TagService(token);
				return tagService.getTags({ projectId: parent._id });
			},
		},
		tagsCount: {
			type: GraphQLInt,
			description: 'Count all project tags',
			resolve(parent, args, { token }) {
				const tagService = new TagService(token);
				return tagService.count({ projectId: parent._id });
			},
		},
	},
};

const configInput = {
	name: 'ProjectInputType',
	description: 'Project Schema base create input type',
	class: 'GraphQLInputObjectType',
	schema: Project.schema,
	exclude: [],
};

const ProjectType = createType(config);
const ProjectInputType = createType(configInput);

export default ProjectType;
export { ProjectInputType };
