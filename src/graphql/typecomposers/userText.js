import { composeWithMongoose } from 'graphql-compose-mongoose'; // GraphQL Composer with Mongoose ORM

import UserText from '../../models/userText';

import UserTC from '../typecomposers/user';

import { RevisionType } from '../types/revision';

import getUserID from '../../lib/getUserID';
import getCurrentCommentaryID from '../../lib/getCurrentCommentaryID';
import removePassageFromURN from '../../lib/removePassageFromURN';

// build Type Composer with model and options
const customizationOptions = {};
const UserTextTC = composeWithMongoose(UserText, customizationOptions);
const UserTextITC = UserTextTC.getITC();

UserTextTC.addRelation('user', {
	resolver: () => UserTC.getResolver('findById'),
	prepareArgs: {
		_id: source => source.userID,
	},
	projection: { userID: true },
});

UserTextTC.addFields({
	latestRevision: {
		type: RevisionType,
		description: 'Returns only the latest revision of the array of revisions',
		resolve(parent) {
			return parent.revisions.reduce(
				(a, b) => (new Date(a.created) > new Date(b.created) ? a : b),
				parent.revisions[0]
			);
		},
	},
});

//
// custom resolvers
//
UserTextTC.setResolver(
	'findMany',
	UserTextTC.get('$findMany').addFilterArg({
		name: 'search',
		type: 'String',
		description:
			'Search by URN, RegEx match of URN without passage citation. filter.urn will be ignored when filter.search is specified.',
		query: (rawQuery, value, resolveParams) => {
			// ignore filter.urn when doing regex match of filter.search
			if (value) {
				delete resolveParams.args.filter.urn;
			}
			const urnFiltered = removePassageFromURN(value);
			rawQuery['$or'] = [{ urn: new RegExp(urnFiltered, 'i') }];
		},
	})
);

UserTextTC.addResolver({
	kind: 'mutation',
	name: 'createOneWithUserID',
	args: {
		userText: UserTextITC,
	},
	type: UserTextTC,
	resolve: async ({ args }) => {
		const userTextInstance = new UserText(args.userText);
		userTextInstance.userID = getUserID();
		if (!args.userText.commentaryID) {
			userTextInstance.commentaryID = await getCurrentCommentaryID();
			if (!userTextInstance.commentaryID) {
				throw Error('Missing CommentaryID');
			}
		}
		const res = await userTextInstance.save();
		return res;
	},
});

// exports
export default UserTextTC;
