import { composeWithMongoose } from 'graphql-compose-mongoose'; // GraphQL Composer with Mongoose ORM

import Project from '../../models/project';

import getUserID from '../../lib/getUserID';


// build Type Composer with model and options
const customizationOptions = {};
const ProjectTC = composeWithMongoose(Project, customizationOptions);
const ProjectITC = ProjectTC.getITC();


//
// custom fields
//


// custom resolvers
ProjectTC.addResolver({
	kind: 'mutation',
	name: 'createAndOwnOne',
	args: {
		project: ProjectITC,
	},
	type: ProjectTC,
	resolve: async ({ args }) => {
		const instance = new Project(args.project);
		const currentUserID = getUserID();
		if (!currentUserID) {
			throw new Error('User not logged in.');
		}
		instance.users.push({
			userId: currentUserID,
			role: 'admin',
		});
		const res = await instance.save();
		return res;
	},
});

// exports
export default ProjectTC;

