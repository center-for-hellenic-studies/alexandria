import { composeWithMongoose } from 'graphql-compose-mongoose'; // GraphQL Composer with Mongoose ORM

import User from '../../models/user';


// build Type Composer with model and options
const customizationOptions = {};
const UserTC = composeWithMongoose(User, customizationOptions);


// exports
export default UserTC;

