import { composeWithMongoose } from 'graphql-compose-mongoose'; // GraphQL Composer with Mongoose ORM

import Group from '../../models/group';
import UserTC from '../typecomposers/user';
import getUserID from '../../lib/getUserID';


// build Type Composer with model and options
const customizationOptions = {};
const GroupTC = composeWithMongoose(Group, customizationOptions);
const GroupITC = GroupTC.getITC();

// associations
GroupTC.addRelation('admins', {
	resolver: () => UserTC.getResolver('findByIds'),
	prepareArgs: {
		_ids: source => source.admins,
		skip: null,
		sort: null,
	},
	projection: { admins: true },
});

GroupTC.addRelation('members', {
	resolver: () => UserTC.getResolver('findByIds'),
	prepareArgs: {
		_ids: source => source.members,
		skip: null,
		sort: null,
	},
	projection: { admins: true },
});

// custom resolvers
GroupTC.addResolver({
	kind: 'mutation',
	name: 'createAndOwnOne',
	args: {
		group: GroupITC,
	},
	type: GroupTC,
	resolve: async ({ args }) => {
		const groupInstance = new Group(args.group);
		const currentUserID = getUserID();
		if (!currentUserID) {
			throw new Error('User not logged in.');
		}
		groupInstance.admins.push(currentUserID);
		const res = await groupInstance.save();
		return res;
	},
});

// exports
export default GroupTC;

