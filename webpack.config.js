const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const env = process.env.NODE_ENV || 'development';
let entry = ['./src/client/index.js'];

if (env === 'development') {
	entry = ['react-hot-loader/patch', 'webpack-hot-middleware/client', ...entry];
}

// Dev notes: Add a key to the config.entry object to build bundle for hydrated
// react, then link to it in the relevant route
// for example, /src/routes/pages.js, search jsFileName

const config = {
	devtool: env === 'development' ? 'cheap-eval-source-map' : false,
	entry: {
		vendor: ['@babel/polyfill', 'react'],
		app: ['./src/components/index.js'],
		main: entry,
	},
	externals: {
		PropTypes: 'prop-types',
		React: 'react',
		ReactDOM: 'react-dom',
	},
	mode: env,
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				use: {
					loader: 'babel-loader',
				},
				exclude: /node_modules/,
			},
			{
				test: /\.css$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							hmr: true,
						},
					},
					'css-loader',
				],
			},
			{
				test: /\.scss$/,
				use: [
					{
						loader: 'style-loader', // creates style nodes from JS strings
					},
					{
						loader: 'css-loader', // translates CSS into CommonJS
					},
					{
						loader: 'sass-loader', // compiles Sass to CSS
					},
				],
			},
			{
				exclude: /node_modules/,
				test: /\.js$/,
				use: 'babel-loader',
			},
		],
	},
	output: {
		filename: '[name].js',
		path: path.join(__dirname, 'build', 'public', 'js'),
		publicPath: '/js',
	},
	plugins: [
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin(),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify(env),
			},
		}),
	],
	resolve: {
		extensions: ['.js', '.jsx', '.json', '.wasm', '.mjs', '*'],

		alias: {
			// it's fine to keep this line in production,
			// because @hot-loader/react-dom is a noop
			// and is very small
			'react-dom': '@hot-loader/react-dom',
		},
	},
};

if (env === 'development') {
	config.plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = config;
