import dotenvSetup from '../src/dotenv';
import setupDB, { closeDB } from '../src/mongoose';

import generateUsers from '../src/__seeds__/user';
import generateProjects from '../src/__seeds__/project';
import generateCollection from '../src/__seeds__/collection';
import generateItem from '../src/__seeds__/item';
// import generateTenant from '../src/__seeds__/tenant';
import generateCommenter from '../src/__seeds__/commenter';

dotenvSetup();

const db = setupDB();

db.on('error', console.error)
	.on('disconnected', setupDB)
	.once('open', async () => {
		console.info(
			`Connected to mongodb ( host: ${db.host}, port: ${db.port}, name: ${db.name} )`
		);

		const ids = {};

		// generateUsers
		try {
			ids.users = await generateUsers(1);
		} catch (err) {
			console.error(err);
		}
		console.info('Generated users');

		// generate projects with users
		try {
			ids.projects = await generateProjects(1, ids.users);
		} catch (err) {
			console.error(err);
		}
		console.info('Generated projects');

		// generate collections for projects with itemSchemas
		try {
			ids.collections = await generateCollection(1000, ids.projects);
		} catch (err) {
			console.error(err);
		}
		console.info('Generated collections');

		// generate items
		try {
			ids.items = await generateItem(10000, ids.projects, ids.collections);
		} catch (err) {
			console.error(err);
		}
		console.info('Generated items');

		// generate tenants
		// try {
		// 	ids.tenantIds = await generateTenant(1);
		// } catch (err) {
		// 	console.error(err);
		// }

		// generate commenters
		try {
			ids.commenterIds = await generateCommenter(1);
		} catch (err) {
			console.error(err);
		}

		// end seed generation process
		db.close(() => {
			console.log('Connection closed');
			process.exit(0);
		});
	});
