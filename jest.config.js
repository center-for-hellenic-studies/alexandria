// dotenv
const dotenvSetup = require('./src/dotenv');

dotenvSetup();

module.exports = {
	collectCoverage: !!process.env.COLLECT_TEST_COVERAGE,
	collectCoverageFrom: ['src/**/*.js'],
	testEnvironment: 'node',
};
