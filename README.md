# New Alexandria Orphe.us GraphQL Server

An API providing data to the New Alexandria frontend. Built with `express` and `graphql`. The application is used for easily making digital collections interfaces. The data model is a simple implementation of a multitenant application that supports many different Projects and their corresponding Collections and Items. User accounts are shared across the application.

## Developing

First, clone this repository. Ensure you have installed `yarn`, node 8.6, mongo, and redis installed on your local machine or configure the appropriate `.env.*.local` file with credentials as necessary to connect to your mongo and redis data sources. Then run the following commands:

Install dependencies:
```bash
yarn
```

Start application:
```bash
yarn start
```

The application GraphiQL API explorer should be available at http://localhost:3001/graphiql.

## Development
The Alexandria API and Client require 4 processes to run the development server.
1. mongod (use the db:setup command to seed development data)
2. redis-server
3. yarn run start-dev 
4. yarn run webpack

The application is based on the Orpheus Design System and uses open source React components published via Bit, which are Server-Side Rendered via the Express.js app, then hydrated after initial page load.  Many of the Orpheus containers initiate Graphql queries using React-apollo.  

Each route in the express app server-side renders an initial page-level component, at /src/routers/pages.js, which will be used to render any content necessary for social media and seo concerns such as meta-tags or descriptions.  

The initial page is rendered via a Handlebars template defined in pages.js and uses the HomeLoading component, then will initate the component reference ni the SSR'd html template in router/pages.js.  When I tried to render the /pages/home component, which has the Orpheus components and containers, I encountered several layers of errors, similar to when babel and webpack weren't configured.

Each page-level route has a component associated with it, for example the '/' route is associated with the /src/pages/HomeLoader component, which is SSR'd, and the /src/pages/Home container, which serves as the root for any React-apollo query that happen in any child containers.

### SSR Example
Code based on article: https://medium.com/@danlegion/react-server-side-rendering-with-express-b6faf56ce22
and repo: https://github.com/danleegion/React-SSR

### Static/loading SSR'd page v. interactive containers
The ideal situation with SSR'd react components is that we'd SSR one component, then hydrate it.  In the current setup, the Express route would render /src/pages/Home, then hydrate it when the page bundles loads.  Currently, the server needs to render a lightweight component and basic metatags instead of the full component primarily because of build processes.

The page bundle, that includes the Orpheus containers, is built using webpack and included in the page in a link tag.  This requires several processes to deal with scss and non-vanilla js.  There's some babel or webpack issue with rendering the Orpheus containers on the server.  

While the current situation with the HomeLoader and Home components may be less than ideal, it does accomplish our needs:
* SSR'd metatags for social
* SSR'd text for SEO
* Authentication on the server
* Ability to directly use Orpheus containers


### To add a new page route
1. Add route to /src/routes/pages.js
2. Add queries in the express route to get data for skeleton page (metatags, title, maybe description content)
3. Render skeleton page using htmlTemplate in routes file, which needs to include the a react element with an id that serves as a loader and will be replaced by components served via linked bundle.  Change jsFileName in the template to point to proper bundle defined in webpack.config
4. Update entry object in webpack.config.js to build a new bundle for the new page. Add the key as the jsFileName in the pages.js template.
5. Create new directory in /src/pages for new route and build layout from there.

### To add a component from Bit
1. use yarn add to add the component, if you want to edit component in place, use the bit import command, then bit watch (see below)
2. import component in react container

### To edit bit components in directory
1. bit init
2. bit import bit.envs/compilers/react -c
3. Ensure the componentsDefaultDirectory value in package.json makes sense (I use /bit-components)
4. bit import (componentname)
5. Edit component as needed, use bit build or bit watch commans
