CLIENT=NODE_ENV=development node_modules/.bin/webpack --watch
SERVER=NODE_ENV=development node_modules/.bin/nodemon src --exec node_modules/.bin/babel-node

.PHONY: build client dev server

dev:
	${SERVER} & ${CLIENT}

build:
	NODE_ENV=production node_modules/.bin/webpack

client:
	${CLIENT}

server:
	${SERVER}
