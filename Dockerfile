# build environment
FROM node:8.16.1-jessie as builder

# ssh git
ARG SSH_PRIVATE_KEY
ARG SSH_SERVER_HOSTKEYS
RUN mkdir /root/.ssh/
RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa
RUN echo "${SSH_SERVER_HOSTKEYS}" > /root/.ssh/known_hosts
RUN chmod 0600 /root/.ssh/id_rsa

# set working directory
RUN mkdir /app
COPY . /app/.
WORKDIR /app

# install and cache app dependencies
RUN yarn

# build
RUN yarn build


# prepare runtime
# downgrade pm2 to @3.5.1
RUN npm install pm2@3.5.1 -g
EXPOSE 80 443 43554
RUN ls
CMD ["pm2-runtime", "start", "pm2.json", "--env", "production", "--web"]
